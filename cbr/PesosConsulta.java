package trabalho.cbr;

public class PesosConsulta {
    Double pesoJogadorMao;
    Double pesoCartaAltaRobo;
    Double pesoCartaMediaRobo;
    Double pesoCartaBaixaRobo;
    Double pesoCartaAltaHumano;
    Double pesoCartaMediaHumano;
    Double pesoCartaBaixaHumano;
    Double pesoPrimeiraCartaRobo;
    Double pesoPrimeiraCartaHumano;
    Double pesoSegundaCartaRobo;
    Double pesoSegundaCartaHumano;
    Double pesoTerceiraCartaRobo;
    Double pesoTerceiraCartaHumano;
    Double pesoGanhadorPrimeiraRodada;
    Double pesoGanhadorSegundaRodada;
    Double pesoGanhadorTerceiraRodada;
    Double pesoRoboCartaVirada;
    Double pesoHumanoCartaVirada;
    Double pesoQuemPediuEnvido;
    Double pesoQuemPediuFaltaEnvido;
    Double pesoQuemPediuRealEnvido;
    Double pesoPontosEnvidoRobo;
    Double pesoPontosEnvidoHumano;
    Double pesoQuemNegouEnvido;
    Double pesoQuemGanhouEnvido;
    Double pesoTentosEnvido;
    Double pesoQuemFlor;
    Double pesoQuemContraFlor;
    Double pesoQuemContraFlorResto;
    Double pesoQuemNegouFlor;
    Double pesoPontosFlorRobo;
    Double pesoPontosFlorHumano;
    Double pesoQuemGanhouFlor;
    Double pesoTentosFlor;
    Double pesoQuemEscondeuPontosEnvido;
    Double pesoQuemEscondeuPontosFlor;
    Double pesoQuemTruco;
    Double pesoQuandoTruco;
    Double pesoQuemRetruco;
    Double pesoQuandoRetruco;
    Double pesoQuemValeQuatro;
    Double pesoQuandoValeQuatro;
    Double pesoQuemNegouTruco;
    Double pesoQuemGanhouTruco;
    Double pesoTentosTruco;
    Double pesoTentosAnterioresRobo;
    Double pesoTentosAnterioresHumano;
    Double pesoTentosPosterioresRobo;
    Double pesoTentosPosterioresHumano;
    Double pesoRoboMentiuEnvido;
    Double pesoHumanoMentiuEnvido;
    Double pesoRoboMentiuFlor;
    Double pesoHumanoMentiuFlor;
    Double pesoRoboMentiuTruco;
    Double pesoHumanoMentiuTruco;
    Double pesoQuemBaralho;

    public PesosConsulta() {
        pesoJogadorMao = 1.0;
        pesoCartaAltaRobo = 1.0;
        pesoCartaMediaRobo = 1.0;
        pesoCartaBaixaRobo = 1.0;
        pesoCartaAltaHumano = 1.0;
        pesoCartaMediaHumano = 1.0;
        pesoCartaBaixaHumano = 1.0;
        pesoPrimeiraCartaRobo = 1.0;
        pesoPrimeiraCartaHumano = 1.0;
        pesoSegundaCartaRobo = 1.0;
        pesoSegundaCartaHumano = 1.0;
        pesoTerceiraCartaRobo = 1.0;
        pesoTerceiraCartaHumano = 1.0;
        pesoGanhadorPrimeiraRodada = 1.0;
        pesoGanhadorSegundaRodada = 1.0;
        pesoGanhadorTerceiraRodada = 1.0;
        pesoRoboCartaVirada = 1.0;
        pesoHumanoCartaVirada = 1.0;
        pesoQuemPediuEnvido = 1.0;
        pesoQuemPediuFaltaEnvido = 1.0;
        pesoQuemPediuRealEnvido = 1.0;
        pesoPontosEnvidoRobo = 1.0;
        pesoPontosEnvidoHumano = 1.0;
        pesoQuemNegouEnvido = 1.0;
        pesoQuemGanhouEnvido = 1.0;
        pesoTentosEnvido = 1.0;
        pesoQuemFlor = 1.0;
        pesoQuemContraFlor = 1.0;
        pesoQuemContraFlorResto = 1.0;
        pesoQuemNegouFlor = 1.0;
        pesoPontosFlorRobo = 1.0;
        pesoPontosFlorHumano = 1.0;
        pesoQuemGanhouFlor = 1.0;
        pesoTentosFlor = 1.0;
        pesoQuemEscondeuPontosEnvido = 1.0;
        pesoQuemEscondeuPontosFlor = 1.0;
        pesoQuemTruco = 1.0;
        pesoQuandoTruco = 1.0;
        pesoQuemRetruco = 1.0;
        pesoQuandoRetruco = 1.0;
        pesoQuemValeQuatro = 1.0;
        pesoQuandoValeQuatro = 1.0;
        pesoQuemNegouTruco = 1.0;
        pesoQuemGanhouTruco = 1.0;
        pesoTentosTruco = 1.0;
        pesoTentosAnterioresRobo = 1.0;
        pesoTentosAnterioresHumano = 1.0;
        pesoTentosPosterioresRobo = 1.0;
        pesoTentosPosterioresHumano = 1.0;
        pesoRoboMentiuEnvido = 1.0;
        pesoHumanoMentiuEnvido = 1.0;
        pesoRoboMentiuFlor = 1.0;
        pesoHumanoMentiuFlor = 1.0;
        pesoRoboMentiuTruco = 1.0;
        pesoHumanoMentiuTruco = 1.0;
        pesoQuemBaralho = 1.0;
    }
    
    public void setPesosEnvido() {
        pesoJogadorMao = 200.0;
        pesoPontosEnvidoRobo = 100.0;
        pesoPrimeiraCartaHumano = 50.0;
    }
    
    public void setPesosTruco() {
        pesoJogadorMao = 75.0;
        pesoCartaAltaRobo = 100.0;
        pesoCartaMediaRobo = 100.0;
        pesoCartaBaixaRobo = 100.0;
        pesoCartaAltaHumano = 25.0;
        pesoCartaMediaHumano = 25.0;
        pesoCartaBaixaHumano = 25.0;
        pesoPrimeiraCartaRobo = 75.0;
        pesoPrimeiraCartaHumano = 75.0;
        pesoSegundaCartaRobo = 50.0;
        pesoSegundaCartaHumano = 50.0;
        pesoTerceiraCartaRobo = 50.0;
        pesoTerceiraCartaHumano = 50.0;
        pesoGanhadorPrimeiraRodada = 200.0;
        pesoGanhadorSegundaRodada = 300.0;
        pesoGanhadorTerceiraRodada = 100.0;
        pesoQuemTruco = 100.0;
    }
    
    public void setPesosCarta() {
        pesoJogadorMao = 300.0;
        pesoCartaAltaRobo = 100.0;
        pesoCartaMediaRobo = 100.0;
        pesoCartaBaixaRobo = 100.0;
        pesoPrimeiraCartaRobo = 100.0;
        pesoSegundaCartaRobo = 100.0;
    }
    
    public void setPesosDefault() {
        pesoCartaAltaRobo = 100.0;
        pesoCartaMediaRobo = 100.0;
        pesoCartaBaixaRobo = 100.0;
    }

    public Double getPesoJogadorMao() {
        return pesoJogadorMao;
    }

    public Double getPesoCartaAltaRobo() {
        return pesoCartaAltaRobo;
    }

    public Double getPesoCartaMediaRobo() {
        return pesoCartaMediaRobo;
    }

    public Double getPesoCartaBaixaRobo() {
        return pesoCartaBaixaRobo;
    }

    public Double getPesoCartaAltaHumano() {
        return pesoCartaAltaHumano;
    }

    public Double getPesoCartaMediaHumano() {
        return pesoCartaMediaHumano;
    }

    public Double getPesoCartaBaixaHumano() {
        return pesoCartaBaixaHumano;
    }

    public Double getPesoPrimeiraCartaRobo() {
        return pesoPrimeiraCartaRobo;
    }

    public Double getPesoPrimeiraCartaHumano() {
        return pesoPrimeiraCartaHumano;
    }

    public Double getPesoSegundaCartaRobo() {
        return pesoSegundaCartaRobo;
    }

    public Double getPesoSegundaCartaHumano() {
        return pesoSegundaCartaHumano;
    }

    public Double getPesoTerceiraCartaRobo() {
        return pesoTerceiraCartaRobo;
    }

    public Double getPesoTerceiraCartaHumano() {
        return pesoTerceiraCartaHumano;
    }

    public Double getPesoGanhadorPrimeiraRodada() {
        return pesoGanhadorPrimeiraRodada;
    }

    public Double getPesoGanhadorSegundaRodada() {
        return pesoGanhadorSegundaRodada;
    }

    public Double getPesoGanhadorTerceiraRodada() {
        return pesoGanhadorTerceiraRodada;
    }

    public Double getPesoRoboCartaVirada() {
        return pesoRoboCartaVirada;
    }

    public Double getPesoHumanoCartaVirada() {
        return pesoHumanoCartaVirada;
    }

    public Double getPesoQuemPediuEnvido() {
        return pesoQuemPediuEnvido;
    }

    public Double getPesoQuemPediuFaltaEnvido() {
        return pesoQuemPediuFaltaEnvido;
    }

    public Double getPesoQuemPediuRealEnvido() {
        return pesoQuemPediuRealEnvido;
    }

    public Double getPesoPontosEnvidoRobo() {
        return pesoPontosEnvidoRobo;
    }

    public Double getPesoPontosEnvidoHumano() {
        return pesoPontosEnvidoHumano;
    }

    public Double getPesoQuemNegouEnvido() {
        return pesoQuemNegouEnvido;
    }

    public Double getPesoQuemGanhouEnvido() {
        return pesoQuemGanhouEnvido;
    }

    public Double getPesoTentosEnvido() {
        return pesoTentosEnvido;
    }

    public Double getPesoQuemFlor() {
        return pesoQuemFlor;
    }

    public Double getPesoQuemContraFlor() {
        return pesoQuemContraFlor;
    }

    public Double getPesoQuemContraFlorResto() {
        return pesoQuemContraFlorResto;
    }

    public Double getPesoQuemNegouFlor() {
        return pesoQuemNegouFlor;
    }

    public Double getPesoPontosFlorRobo() {
        return pesoPontosFlorRobo;
    }

    public Double getPesoPontosFlorHumano() {
        return pesoPontosFlorHumano;
    }

    public Double getPesoQuemGanhouFlor() {
        return pesoQuemGanhouFlor;
    }

    public Double getPesoTentosFlor() {
        return pesoTentosFlor;
    }

    public Double getPesoQuemEscondeuPontosEnvido() {
        return pesoQuemEscondeuPontosEnvido;
    }

    public Double getPesoQuemEscondeuPontosFlor() {
        return pesoQuemEscondeuPontosFlor;
    }

    public Double getPesoQuemTruco() {
        return pesoQuemTruco;
    }

    public Double getPesoQuandoTruco() {
        return pesoQuandoTruco;
    }

    public Double getPesoQuemRetruco() {
        return pesoQuemRetruco;
    }

    public Double getPesoQuandoRetruco() {
        return pesoQuandoRetruco;
    }

    public Double getPesoQuemValeQuatro() {
        return pesoQuemValeQuatro;
    }

    public Double getPesoQuandoValeQuatro() {
        return pesoQuandoValeQuatro;
    }

    public Double getPesoQuemNegouTruco() {
        return pesoQuemNegouTruco;
    }

    public Double getPesoQuemGanhouTruco() {
        return pesoQuemGanhouTruco;
    }

    public Double getPesoTentosTruco() {
        return pesoTentosTruco;
    }

    public Double getPesoTentosAnterioresRobo() {
        return pesoTentosAnterioresRobo;
    }

    public Double getPesoTentosAnterioresHumano() {
        return pesoTentosAnterioresHumano;
    }

    public Double getPesoTentosPosterioresRobo() {
        return pesoTentosPosterioresRobo;
    }

    public Double getPesoTentosPosterioresHumano() {
        return pesoTentosPosterioresHumano;
    }

    public Double getPesoRoboMentiuEnvido() {
        return pesoRoboMentiuEnvido;
    }

    public Double getPesoHumanoMentiuEnvido() {
        return pesoHumanoMentiuEnvido;
    }

    public Double getPesoRoboMentiuFlor() {
        return pesoRoboMentiuFlor;
    }

    public Double getPesoHumanoMentiuFlor() {
        return pesoHumanoMentiuFlor;
    }

    public Double getPesoRoboMentiuTruco() {
        return pesoRoboMentiuTruco;
    }

    public Double getPesoHumanoMentiuTruco() {
        return pesoHumanoMentiuTruco;
    }

    public Double getPesoQuemBaralho() {
        return pesoQuemBaralho;
    }

}
