package trabalho.cbr;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import biz.aduna.map.cluster.gui.MenuBar.ExitAction;
import jcolibri.casebase.LinealCaseBase;
import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRCaseBase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.cbrcore.Connector;
import jcolibri.connector.PlainTextConnector;
import jcolibri.exception.ExecutionException;
import jcolibri.method.retrieve.RetrievalResult;
import jcolibri.method.retrieve.NNretrieval.NNConfig;
import jcolibri.method.retrieve.NNretrieval.NNScoringMethod;
import jcolibri.method.retrieve.NNretrieval.similarity.global.Average;
import jcolibri.method.retrieve.NNretrieval.similarity.local.Interval;
import jcolibri.method.retrieve.NNretrieval.similarity.local.Equal;
import jcolibri.method.retrieve.selection.SelectCases;

public class TrucoCBR {

    Connector _connector;
    CBRCaseBase _caseBase;
    TrucoDescription current_best_case = null;
    TrucoDescription current_game_state = null;
    
    int current_tipo_consulta = 0;
    static final int DEFAULT = 0; 
    static final int ENVIDO = 1; 
    static final int TRUCO = 2; 
    static final int CARTA = 3; 

    
    public void initialize() throws ExecutionException {
        try{
            _connector = new PlainTextConnector();
            _connector.initFromXMLfile(jcolibri.util.FileIO.findFile("trabalho/cbr/plaintextconfig.xml"));
            _caseBase  = new LinealCaseBase();
        } catch (Exception e){
            throw new ExecutionException(e);
        }
    }

    public CBRCaseBase openConnection() throws ExecutionException {
        _caseBase.init(_connector);
//        java.util.Collection<CBRCase> cases = _caseBase.getCases();
//        TrucoDescription t = null;
//        for(CBRCase c: cases) {
//            t = (TrucoDescription) c.getDescription();
//            System.out.println(t.getQuemTruco());
//        }
        return _caseBase;
    }

    public void closeConnection() throws ExecutionException {
        _connector.close();
    }

    public Collection<RetrievalResult> executeQuery(CBRQuery query, int tipoConsulta) throws ExecutionException {
        NNConfig simConfig = new NNConfig();
        simConfig.setDescriptionSimFunction(new Average());

        PesosConsulta pesos = new PesosConsulta();
        if (tipoConsulta == ENVIDO) {
            pesos.setPesosEnvido();
        } else if(tipoConsulta == TRUCO) {
            pesos.setPesosTruco();
        } else if(tipoConsulta == CARTA) {
            pesos.setPesosCarta();
        } else {
            pesos.setPesosDefault();
        }

        simConfig.addMapping(new Attribute("JogadorMao", TrucoDescription.class), new Equal()); // jogador

        Attribute cartaAltaRobo = new Attribute("CartaAltaRobo", TrucoDescription.class); 
        simConfig.addMapping(cartaAltaRobo, new Interval(130)); // carta
        simConfig.setWeight(cartaAltaRobo, pesos.getPesoCartaAltaRobo());
        Attribute cartaMediaRobo = new Attribute("CartaMediaRobo", TrucoDescription.class);
        simConfig.addMapping(cartaMediaRobo, new Interval(130)); // carta
        simConfig.setWeight(cartaMediaRobo, pesos.getPesoCartaMediaRobo());
        Attribute cartaBaixaRobo = new Attribute("CartaBaixaRobo", TrucoDescription.class);
        simConfig.addMapping(cartaBaixaRobo, new Interval(130)); // carta
        simConfig.setWeight(cartaBaixaRobo, pesos.getPesoCartaBaixaRobo());
        Attribute cartaAltaHumano = new Attribute("CartaAltaHumano", TrucoDescription.class);
        simConfig.addMapping(cartaAltaHumano, new Interval(130)); // carta
        simConfig.setWeight(cartaAltaHumano, pesos.getPesoCartaAltaHumano());
        Attribute cartaMediaHumano = new Attribute("CartaMediaHumano", TrucoDescription.class);
        simConfig.addMapping(cartaMediaHumano, new Interval(130)); // carta
        simConfig.setWeight(cartaMediaHumano, pesos.getPesoCartaMediaHumano());
        Attribute cartaBaixaHumano = new Attribute("CartaBaixaHumano", TrucoDescription.class);
        simConfig.addMapping(cartaBaixaHumano, new Interval(130)); // carta
        simConfig.setWeight(cartaBaixaHumano, pesos.getPesoCartaBaixaHumano());
        Attribute primeiraCartaRobo = new Attribute("PrimeiraCartaRobo", TrucoDescription.class);
        simConfig.addMapping(primeiraCartaRobo, new Interval(130)); // carta
        simConfig.setWeight(primeiraCartaRobo, pesos.getPesoPrimeiraCartaRobo());
        Attribute primeiraCartaHumano = new Attribute("PrimeiraCartaHumano", TrucoDescription.class);
        simConfig.addMapping(primeiraCartaHumano, new Interval(130)); // carta
        simConfig.setWeight(primeiraCartaHumano, pesos.getPesoPrimeiraCartaHumano());
        Attribute segundaCartaRobo = new Attribute("SegundaCartaRobo", TrucoDescription.class);
        simConfig.addMapping(segundaCartaRobo, new Interval(130)); // carta
        simConfig.setWeight(segundaCartaRobo, pesos.getPesoSegundaCartaRobo());
        Attribute segundaCartaHumano = new Attribute("SegundaCartaHumano", TrucoDescription.class);
        simConfig.addMapping(segundaCartaHumano, new Interval(130)); // carta
        simConfig.setWeight(segundaCartaHumano, pesos.getPesoSegundaCartaHumano());
        Attribute terceiraCartaRobo = new Attribute("TerceiraCartaRobo", TrucoDescription.class);
        simConfig.addMapping(terceiraCartaRobo, new Interval(130)); // carta
        simConfig.setWeight(terceiraCartaRobo, pesos.getPesoTerceiraCartaRobo());
        Attribute terceiraCartaHumano = new Attribute("TerceiraCartaHumano", TrucoDescription.class);
        simConfig.addMapping(terceiraCartaHumano, new Interval(130)); // carta
        simConfig.setWeight(terceiraCartaHumano, pesos.getPesoTerceiraCartaHumano());
        Attribute ganhadorPrimeiraRodada = new Attribute("GanhadorPrimeiraRodada", TrucoDescription.class);
        simConfig.addMapping(ganhadorPrimeiraRodada, new Equal());//Interval(2)); // jogador
        simConfig.setWeight(ganhadorPrimeiraRodada, pesos.getPesoGanhadorPrimeiraRodada());
        Attribute ganhadorSegundaRodada = new Attribute("GanhadorSegundaRodada", TrucoDescription.class);
        simConfig.addMapping(ganhadorSegundaRodada, new Equal());//Interval(2)); // jogador
        simConfig.setWeight(ganhadorSegundaRodada, pesos.getPesoGanhadorSegundaRodada());
        Attribute ganhadorTerceiraRodada = new Attribute("GanhadorTerceiraRodada", TrucoDescription.class);
        simConfig.addMapping(ganhadorTerceiraRodada, new Equal());//Interval(2)); // jogador
        simConfig.setWeight(ganhadorTerceiraRodada, pesos.getPesoGanhadorTerceiraRodada());
        Attribute roboCartaVirada = new Attribute("RoboCartaVirada", TrucoDescription.class);
        simConfig.addMapping(roboCartaVirada, new Equal()); // rodada
        simConfig.setWeight(roboCartaVirada, pesos.getPesoRoboCartaVirada());
        Attribute humanoCartaVirada = new Attribute("HumanoCartaVirada", TrucoDescription.class);
        simConfig.addMapping(humanoCartaVirada, new Equal()); // rodada
        simConfig.setWeight(humanoCartaVirada, pesos.getPesoHumanoCartaVirada());
        Attribute quemPediuEnvido = new Attribute("QuemPediuEnvido", TrucoDescription.class);
        simConfig.addMapping(quemPediuEnvido, new Equal()); // jogador
        simConfig.setWeight(quemPediuEnvido, pesos.getPesoQuemPediuEnvido());
        Attribute quemPediuFaltaEnvido = new Attribute("QuemPediuFaltaEnvido", TrucoDescription.class);
        simConfig.addMapping(quemPediuFaltaEnvido, new Equal()); // jogador
        simConfig.setWeight(quemPediuFaltaEnvido, pesos.getPesoQuemPediuFaltaEnvido());
        Attribute quemPediuRealEnvido = new Attribute("QuemPediuRealEnvido", TrucoDescription.class);
        simConfig.addMapping(quemPediuRealEnvido, new Equal()); // jogador
        simConfig.setWeight(quemPediuRealEnvido, pesos.getPesoQuemPediuRealEnvido());
        Attribute pontosEnvidoRobo = new Attribute("PontosEnvidoRobo", TrucoDescription.class);
        simConfig.addMapping(pontosEnvidoRobo, new Interval(33)); // numero
        simConfig.setWeight(pontosEnvidoRobo, pesos.getPesoPontosEnvidoRobo());
        Attribute pontosEnvidoHumano = new Attribute("PontosEnvidoHumano", TrucoDescription.class);
        simConfig.addMapping(pontosEnvidoHumano, new Interval(33)); // numero
        simConfig.setWeight(pontosEnvidoHumano, pesos.getPesoPontosEnvidoHumano());
        Attribute quemNegouEnvido = new Attribute("QuemNegouEnvido", TrucoDescription.class);
        simConfig.addMapping(quemNegouEnvido, new Equal()); // jogador 6
        simConfig.setWeight(quemNegouEnvido, pesos.getPesoQuemNegouEnvido());
        Attribute quemGanhouEnvido = new Attribute("QuemGanhouEnvido", TrucoDescription.class);
        simConfig.addMapping(quemGanhouEnvido, new Equal()); // jogador
        simConfig.setWeight(quemGanhouEnvido, pesos.getPesoQuemGanhouEnvido());
        Attribute tentosEnvido = new Attribute("TentosEnvido", TrucoDescription.class);
        simConfig.addMapping(tentosEnvido, new Equal()); // numero
        simConfig.setWeight(tentosEnvido, pesos.getPesoTentosEnvido());
        Attribute quemFlor = new Attribute("QuemFlor", TrucoDescription.class);
        simConfig.addMapping(quemFlor, new Equal()); // jogador
        simConfig.setWeight(quemFlor, pesos.getPesoQuemFlor());
        Attribute quemContraFlor = new Attribute("QuemContraFlor", TrucoDescription.class);
        simConfig.addMapping(quemContraFlor, new Equal()); // jogador
        simConfig.setWeight(quemContraFlor, pesos.getPesoQuemContraFlor());
        Attribute quemContraFlorResto = new Attribute("QuemContraFlorResto", TrucoDescription.class);
        simConfig.addMapping(quemContraFlorResto, new Equal()); // jogador
        simConfig.setWeight(quemContraFlorResto, pesos.getPesoQuemContraFlorResto());
        Attribute quemNegouFlor = new Attribute("QuemNegouFlor", TrucoDescription.class);
        simConfig.addMapping(quemNegouFlor, new Equal()); // jogador 6
        simConfig.setWeight(quemNegouFlor, pesos.getPesoQuemNegouFlor());
        Attribute pontosFlorRobo = new Attribute("PontosFlorRobo", TrucoDescription.class);
        simConfig.addMapping(pontosFlorRobo, new Equal()); // numero
        simConfig.setWeight(pontosFlorRobo, pesos.getPesoPontosFlorRobo());
        Attribute pontosFlorHumano = new Attribute("PontosFlorHumano", TrucoDescription.class);
        simConfig.addMapping(pontosFlorHumano, new Equal()); // numero
        simConfig.setWeight(pontosFlorHumano, pesos.getPesoPontosFlorHumano());
        Attribute quemGanhouFlor = new Attribute("QuemGanhouFlor", TrucoDescription.class);
        simConfig.addMapping(quemGanhouFlor, new Equal()); // jogador
        simConfig.setWeight(quemGanhouFlor, pesos.getPesoQuemGanhouFlor());
        Attribute tentosFlor = new Attribute("TentosFlor", TrucoDescription.class);
        simConfig.addMapping(tentosFlor, new Equal()); // numero
        simConfig.setWeight(tentosFlor, pesos.getPesoTentosFlor());
        Attribute quemEscondeuPontosEnvido = new Attribute("QuemEscondeuPontosEnvido", TrucoDescription.class);
        simConfig.addMapping(quemEscondeuPontosEnvido, new Equal()); // jogador
        simConfig.setWeight(quemEscondeuPontosEnvido, pesos.getPesoQuemEscondeuPontosEnvido());
        Attribute quemEscondeuPontosFlor = new Attribute("QuemEscondeuPontosFlor", TrucoDescription.class);
        simConfig.addMapping(quemEscondeuPontosFlor, new Equal()); // jogador
        simConfig.setWeight(quemEscondeuPontosFlor, pesos.getPesoQuemEscondeuPontosFlor());
        Attribute quemTruco = new Attribute("QuemTruco", TrucoDescription.class);
        simConfig.addMapping(quemTruco, new Equal()); // jogador
        simConfig.setWeight(quemTruco, pesos.getPesoQuemTruco());
        Attribute quandoTruco = new Attribute("QuandoTruco", TrucoDescription.class);
        simConfig.addMapping(quandoTruco, new Equal()); // rodada
        simConfig.setWeight(quandoTruco, pesos.getPesoQuandoTruco());
        Attribute quemRetruco = new Attribute("QuemRetruco", TrucoDescription.class);
        simConfig.addMapping(quemRetruco, new Equal()); // jogador
        simConfig.setWeight(quemRetruco, pesos.getPesoQuemRetruco());
        Attribute quandoRetruco = new Attribute("QuandoRetruco", TrucoDescription.class);
        simConfig.addMapping(quandoRetruco, new Equal()); // rodada
        simConfig.setWeight(quandoRetruco, pesos.getPesoQuandoRetruco());
        Attribute quemValeQuatro = new Attribute("QuemValeQuatro", TrucoDescription.class);
        simConfig.addMapping(quemValeQuatro, new Equal()); // jogador
        simConfig.setWeight(quemValeQuatro, pesos.getPesoQuemValeQuatro());
        Attribute quandoValeQuatro = new Attribute("QuandoValeQuatro", TrucoDescription.class);
        simConfig.addMapping(quandoValeQuatro, new Equal()); // rodada
        simConfig.setWeight(quandoValeQuatro, pesos.getPesoQuandoValeQuatro());
        Attribute quemNegouTruco = new Attribute("QuemNegouTruco", TrucoDescription.class);
        simConfig.addMapping(quemNegouTruco, new Equal()); // jogador 6
        simConfig.setWeight(quemNegouTruco, pesos.getPesoQuemNegouTruco());
        Attribute quemGanhouTruco = new Attribute("QuemGanhouTruco", TrucoDescription.class);
        simConfig.addMapping(quemGanhouTruco, new Equal()); // jogador
        simConfig.setWeight(quemGanhouTruco, pesos.getPesoQuemGanhouTruco());
        Attribute tentosTruco = new Attribute("TentosTruco", TrucoDescription.class);
        simConfig.addMapping(tentosTruco, new Equal()); // numero
        simConfig.setWeight(tentosTruco, pesos.getPesoTentosTruco());
        Attribute tentosAnterioresRobo = new Attribute("TentosAnterioresRobo", TrucoDescription.class);
        simConfig.addMapping(tentosAnterioresRobo, new Equal()); // numero
        simConfig.setWeight(tentosAnterioresRobo, pesos.getPesoTentosAnterioresRobo());
        Attribute tentosAnterioresHumano = new Attribute("TentosAnterioresHumano", TrucoDescription.class);
        simConfig.addMapping(tentosAnterioresHumano, new Equal()); // numero
        simConfig.setWeight(tentosAnterioresHumano, pesos.getPesoTentosAnterioresHumano());
        Attribute tentosPosterioresRobo = new Attribute("TentosPosterioresRobo", TrucoDescription.class);
        simConfig.addMapping(tentosPosterioresRobo, new Equal()); // numero
        simConfig.setWeight(tentosPosterioresRobo, pesos.getPesoTentosPosterioresRobo());
        Attribute tentosPosterioresHumano = new Attribute("TentosPosterioresHumano", TrucoDescription.class);
        simConfig.addMapping(tentosPosterioresHumano, new Equal()); // numero
        simConfig.setWeight(tentosPosterioresHumano, pesos.getPesoTentosPosterioresHumano());
        Attribute roboMentiuEnvido = new Attribute("RoboMentiuEnvido", TrucoDescription.class);
        simConfig.addMapping(roboMentiuEnvido, new Equal()); // bool
        simConfig.setWeight(roboMentiuEnvido, pesos.getPesoRoboMentiuEnvido());
        Attribute humanoMentiuEnvido = new Attribute("HumanoMentiuEnvido", TrucoDescription.class);
        simConfig.addMapping(humanoMentiuEnvido, new Equal()); // bool
        simConfig.setWeight(humanoMentiuEnvido, pesos.getPesoHumanoMentiuEnvido());
        Attribute roboMentiuFlor = new Attribute("RoboMentiuFlor", TrucoDescription.class);
        simConfig.addMapping(roboMentiuFlor, new Equal()); // bool
        simConfig.setWeight(roboMentiuFlor, pesos.getPesoRoboMentiuFlor());
        Attribute humanoMentiuFlor = new Attribute("HumanoMentiuFlor", TrucoDescription.class);
        simConfig.addMapping(humanoMentiuFlor, new Equal()); // bool
        simConfig.setWeight(humanoMentiuFlor, pesos.getPesoHumanoMentiuFlor());
        Attribute roboMentiuTruco = new Attribute("RoboMentiuTruco", TrucoDescription.class);
        simConfig.addMapping(roboMentiuTruco, new Equal()); // bool
        simConfig.setWeight(roboMentiuTruco, pesos.getPesoRoboMentiuTruco());
        Attribute humanoMentiuTruco = new Attribute("HumanoMentiuTruco", TrucoDescription.class);
        simConfig.addMapping(humanoMentiuTruco, new Equal()); // bool
        simConfig.setWeight(humanoMentiuTruco, pesos.getPesoHumanoMentiuTruco());
        Attribute quemBaralho = new Attribute("QuemBaralho", TrucoDescription.class);
        simConfig.addMapping(quemBaralho, new Equal()); // jogador 6
        simConfig.setWeight(quemBaralho, pesos.getPesoQuemBaralho());

        //        System.out.println("Query Description:");
        //        System.out.println(query.getDescription());
        //        System.out.println();

        Collection<RetrievalResult> eval = NNScoringMethod.evaluateSimilarity(_caseBase.getCases(), query, simConfig);
        
        // Select k cases
        eval = SelectCases.selectTopKRR(eval, 5);
        
        // Print the retrieval
        // System.out.println("Retrieved cases:");
//        for(RetrievalResult nse: eval) {
//            TrucoDescription result = (TrucoDescription) nse.get_case().getDescription();
//            // System.out.println(result.getCASEID()+" -> "+nse.getEval());
//        }
        
        // save best case
        // best_case = eval.get(0);
        return eval;
    }
    
    public TrucoCBR() {
        try {
            initialize();
            openConnection();
        } catch (ExecutionException e) {
            org.apache.commons.logging.LogFactory.getLog(TrucoCBR.class).error(e);
        }
    }

    private TrucoDescription getBestResult(TrucoDescription game_state, int tipoConsulta) {
        boolean nova_consulta = true; 
        // verifica se estado atual já foi setado
        if (current_best_case != null) {
            // verifica se o estado atual e o que foi passado pelo jogo são iguais
            boolean the_same = game_state.equals(current_game_state);
            // System.out.println("Devo reaproveitar a ultima consulta?");
            // System.out.println(the_same + " && " + (tipoConsulta == current_tipo_consulta));
            if (the_same && (tipoConsulta == current_tipo_consulta)) {
                nova_consulta = false;
            }
        }
        if (nova_consulta) {
            System.out.println("Fazendo uma nova consulta...");
            try {
                TrucoDescription queryDesc = game_state;
                CBRQuery query = new CBRQuery();
                query.setDescription(queryDesc);
                Collection<RetrievalResult> results = executeQuery(query, tipoConsulta);

                current_best_case = (TrucoDescription) results.iterator().next().get_case().getDescription();
                System.out.print("Melhor caso encontrado: "+current_best_case.CASEID);
                System.out.println(" com similaridade: "+results.iterator().next().getEval());
            } catch (ExecutionException e) {
                org.apache.commons.logging.LogFactory.getLog(TrucoCBR.class).error(e);
            }
        }
        current_game_state = new TrucoDescription(game_state);
        current_tipo_consulta = tipoConsulta;
        // System.out.println("Caso retornado: "+current_best_case.CASEID);
        return current_best_case;
    }

    public boolean chamarEnvido(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando chamarEnvido. ->");
        TrucoDescription best = getBestResult(gamestate, ENVIDO);
        try {
            if ((best.getQuemPediuEnvido() == 1) || (best.getQuemPediuEnvido() == 2 && best.getQuemNegouEnvido() != 0)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            } 
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean chamarRealEnvido(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando chamarRealEnvido. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, ENVIDO);
            if ((best.getQuemPediuRealEnvido() == 1) || (best.getQuemPediuRealEnvido() == 2 && best.getQuemNegouEnvido() != 4)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean chamarFaltaEnvido(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando chamarFaltaEnvido. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, ENVIDO);
            if ((best.getQuemPediuFaltaEnvido() == 1) || (best.getQuemPediuFaltaEnvido() == 2 && best.getQuemNegouEnvido() != 2)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean aceitarEnvido(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando aceitarEnvido. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, ENVIDO);
            // se o humano pediu envido e o robo nao negou, significa que ele aceitou
            if ((best.getQuemPediuEnvido() == 1) || (best.getQuemPediuEnvido() == 2 && best.getQuemNegouEnvido() != 0)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            } 
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean aceitarRealEnvido(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando aceitarRealEnvido. ->");
        TrucoDescription best = getBestResult(gamestate, ENVIDO);
        try{
            // se o humano pediu real envido e o robo nao negou, significa que ele aceitou
            if ((best.getQuemPediuRealEnvido() == 1) || (best.getQuemPediuRealEnvido() == 2 && best.getQuemNegouEnvido() != 4)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean aceitarFaltaEnvido(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando aceitarFaltaEnvido. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, ENVIDO);
            // se o humano pediu falta envido e o robo nao negou, significa que ele aceitou
            if ((best.getQuemPediuFaltaEnvido() == 1) || (best.getQuemPediuFaltaEnvido() == 2 && best.getQuemNegouEnvido() != 2)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean chamarTruco(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando chamarTruco. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, TRUCO);
            if (((best.getQuemTruco() == 2 && best.getQuemNegouTruco() != 0 ) || (best.getQuemTruco() == 1)) && (best.getQuandoTruco() <= rodada)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean chamarRetruco(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando chamarRetruco. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, TRUCO);
            if (((best.getQuemRetruco() == 2 && best.getQuemNegouTruco() != 2 ) || (best.getQuemRetruco() == 1)) && (best.getQuandoRetruco() <= rodada)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean chamarValeQuatro(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando chamarValeQuatro. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, TRUCO);
            if (((best.getQuemValeQuatro() == 2 && best.getQuemNegouTruco() != 4 ) || (best.getQuemValeQuatro() == 1)) && (best.getQuandoValeQuatro() <= rodada)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean aceitarTruco(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando aceitarTruco. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, TRUCO);
            // se o humano pediu truco e o robo nao negou, significa que ele aceitou
            
            if ((best.getQuemTruco() == 2 && best.getQuemNegouTruco() != 0) || (best.getQuemTruco() == 1 && best.getQuandoTruco() <= rodada)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean aceitarRetruco(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando aceitarRetruco. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, TRUCO);
            // se o humano pediu truco e o robo nao negou, significa que ele aceitou
            if ((best.getQuemRetruco() == 2 && best.getQuemNegouTruco() != 2) || (best.getQuemRetruco() == 1 && best.getQuandoRetruco() <= rodada)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public boolean aceitarValeQuatro(TrucoDescription gamestate, int rodada) {
        System.out.print("Consultando aceitarValeQuatro. ->");
        try {
            TrucoDescription best = getBestResult(gamestate, TRUCO);
            // se o humano pediu truco e o robo nao negou, significa que ele aceitou
            if ((best.getQuemValeQuatro() == 2 && best.getQuemNegouTruco() != 4) || (best.getQuemValeQuatro() == 1 && best.getQuandoValeQuatro() <= rodada)) {
                System.out.println(" True.");
                return true;
            } else {
                System.out.println(" False.");
                return false;
            }
        } catch (Exception e) {
            System.out.println(" False.");
            return false;
        }
    }

    public int primeiraCarta(TrucoDescription gamestate, int rodada) {
        System.out.println("Consultando primeiraCarta. ");
        try {
            TrucoDescription best = getBestResult(gamestate, CARTA);
            int alta  = best.getCartaAltaRobo();
            int media = best.getCartaMediaRobo();
            
            if (best.getPrimeiraCartaRobo() == alta) {
                return gamestate.getCartaAltaRobo();
            } else if(best.getPrimeiraCartaRobo() == media) {
                return gamestate.getCartaMediaRobo();
            } else {
                return gamestate.getCartaBaixaRobo();
            }
        } catch (Exception e) {
            return gamestate.getCartaBaixaRobo();
        }
    }
    
    public int segundaCarta(TrucoDescription gamestate, int rodada) {
        System.out.println("Consultando segundaCarta. ");

        TrucoDescription best = getBestResult(gamestate, CARTA);
        int alta  = best.getCartaAltaRobo();
        int media = best.getCartaMediaRobo();
        
        int primeira = gamestate.getPrimeiraCartaRobo();
        
        try {
            // se a sugestao for jogar a alta e a alta nao foi jogada na primeira rodada
            if (best.getSegundaCartaRobo() == alta && alta != primeira) {
                // retorna carta alta
                return gamestate.getCartaAltaRobo();
            // se a sugestao for jogar a media e a media nao foi jogada na primeira rodada
            } else if(best.getSegundaCartaRobo() == media && media != primeira) {
                // retorna carta media
                return gamestate.getCartaMediaRobo();
            } else {
                // ultima opcao eh a carta baixa, se ela nao foi jogada na primeira
                if (gamestate.getCartaBaixaRobo() != primeira) {
                    return gamestate.getCartaBaixaRobo();
                } else {
                    // se ela foi jogada na primeira rodada, sugere que jogue a media
                    return gamestate.getCartaMediaRobo();
                }
            }
        } catch (Exception e) {
            // se ocorrer um erro, e a primeira carta jogada nao foi a mais baixa
            if (gamestate.getCartaBaixaRobo() != primeira) {
                // manda jogar a baixa
                return gamestate.getCartaBaixaRobo();
            } else {
                // senao manda jogar a media
                return gamestate.getCartaMediaRobo();
            }
        }
    }
    
    public int terceiraCarta(TrucoDescription gamestate, int rodada) {
        System.out.println("Consultando terceiraCarta. ");
        TrucoDescription best = getBestResult(gamestate, CARTA);
        int alta  = best.getCartaAltaRobo();
        int media = best.getCartaMediaRobo();
        int baixa = best.getCartaBaixaRobo();

        int primeira = gamestate.getPrimeiraCartaRobo();
        int segunda  = gamestate.getSegundaCartaRobo();
        
        try {
            
            if (best.getTerceiraCartaRobo() == alta && alta != primeira && alta != segunda) {
                return gamestate.getCartaAltaRobo();
            } else if(best.getTerceiraCartaRobo() == media && media != primeira && media != segunda) {
                return gamestate.getCartaMediaRobo();
            } else {
                if (baixa != primeira && baixa != segunda) {
                    return gamestate.getCartaBaixaRobo();
                } else if (gamestate.getCartaMediaRobo() != primeira && gamestate.getCartaMediaRobo() != segunda) {
                    return gamestate.getCartaMediaRobo();
                } else {
                    return gamestate.getCartaAltaRobo();
                }
            }
        } catch (Exception e) {
            if (gamestate.getCartaBaixaRobo() != primeira && gamestate.getCartaBaixaRobo() != segunda) {
                return gamestate.getCartaBaixaRobo();
            } else if (gamestate.getCartaMediaRobo() != primeira && gamestate.getCartaMediaRobo() != segunda) {
                return gamestate.getCartaMediaRobo();
            } else {
                return gamestate.getCartaAltaRobo();
            }
        }
    }


    public boolean irAoBaralho(TrucoDescription gamestate, int rodada) {
        // 3: Jogador Robô, na 1ª rodada
        // 4: Jogador Robô, na 2ª rodada
        // 5: Jogador Robô, na 3ª rodada
        try {
            TrucoDescription best = getBestResult(gamestate, DEFAULT);
            
            if (best.getQuemBaralho() == 2+rodada) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static void main(String[] args) {
        TrucoCBR cbr = new TrucoCBR();
        List<Integer> list = Arrays.asList(
            null,// CASEID
            2,// JogadorMao
            100,//110,// CartaAltaRobo
            92,//83,// CartaMediaRobo
            70,//50,// CartaBaixaRobo
            82,// CartaAltaHumano
            71,// CartaMediaHumano
            53,// CartaBaixaHumano
            83,// PrimeiraCartaRobo
            53,// PrimeiraCartaHumano
            50,// SegundaCartaRobo
            71,// SegundaCartaHumano
            110,// TerceiraCartaRobo
            82,// TerceiraCartaHumano
            1,// GanhadorPrimeiraRodada
            2,// GanhadorSegundaRodada
            1,// GanhadorTerceiraRodada
            0,// RoboCartaVirada
            0,// HumanoCartaVirada
            0,// QuemPediuEnvido
            0,// QuemPediuFaltaEnvido
            0,// QuemPediuRealEnvido
            null,// PontosEnvidoRobo
            null,// PontosEnvidoHumano
            null,// QuemNegouEnvido
            0,// QuemGanhouEnvido
            0,// TentosEnvido
            0,// QuemFlor
            0,// QuemContraFlor
            0,// QuemContraFlorResto
            null,// QuemNegouFlor
            null,// PontosFlorRobo
            null,// PontosFlorHumano
            0,// QuemGanhouFlor
            0,// TentosFlor
            0,// QuemEscondeuPontosEnvido
            0,// QuemEscondeuPontosFlor
            2,// QuemTruco
            2,// QuandoTruco
            1,// QuemRetruco
            3,// QuandoRetruco
            0,// QuemValeQuatro
            0,// QuandoValeQuatro
            null,// QuemNegouTruco
            1,// QuemGanhouTruco
            3,// TentosTruco
            3,// TentosAnterioresRobo
            0,// TentosAnterioresHumano
            6,// TentosPosterioresRobo
            0,// TentosPosterioresHumano
            0,// RoboMentiuEnvido
            0,// HumanoMentiuEnvido
            0,// RoboMentiuFlor
            0,// HumanoMentiuFlor
            0,// RoboMentiuTruco
            0,// HumanoMentiuTruco
            null// QuemBaralho
        );
        
        TrucoDescription state = new TrucoDescription(list);
        System.out.println("Testando:");

        TrucoDescription best = cbr.getBestResult(state, DEFAULT);
        System.out.println("ID: "+best.CASEID);

        System.out.println("Mao: " + state.getCartaBaixaRobo() + ", " + state.getCartaMediaRobo() +", " + state.getCartaAltaRobo());
        
        System.out.println("\tChamar Envido: " + cbr.chamarEnvido(state, 1));
        System.out.println("\tChamar Real Envido: " + cbr.chamarRealEnvido(state, 1));
        System.out.println("\tChamar Falta Envido: " + cbr.chamarFaltaEnvido(state, 1));
        
        System.out.println("\tAceitar Envido: " + cbr.aceitarEnvido(state, 1));
        System.out.println("\tAceitar Real Envido: " + cbr.aceitarRealEnvido(state, 1));
        System.out.println("\tAceitar Falta Envido: " + cbr.aceitarFaltaEnvido(state, 1));
        
        System.out.println("\tChamar Truco 1: " + cbr.chamarTruco(state, 1));
        System.out.println("\tChamar Truco 2: " + cbr.chamarTruco(state, 2));
        System.out.println("\tChamar Truco 3: " + cbr.chamarTruco(state, 3));
        System.out.println("\tChamar Retruco 1: " + cbr.chamarRetruco(state, 1));
        System.out.println("\tChamar Retruco 2: " + cbr.chamarRetruco(state, 2));
        System.out.println("\tChamar Retruco 3: " + cbr.chamarRetruco(state, 3));
        
        System.out.println("\tó, alterei o estado");
        state.setJogadorMao(1);
        System.out.println("\tChamar Vale Quatro 1: " + cbr.chamarValeQuatro(state, 1));
        System.out.println("\tChamar Vale Quatro 2: " + cbr.chamarValeQuatro(state, 2));
        System.out.println("\tChamar Vale Quatro 3: " + cbr.chamarValeQuatro(state, 3));

        System.out.println("\tAceitar Truco: " + cbr.aceitarTruco(state, 1));
        System.out.println("\tAceitar Retruco: " + cbr.aceitarRetruco(state, 1));
        System.out.println("\tAceitar Vale Quatro: " + cbr.aceitarValeQuatro(state, 1));

        System.out.println("\tPrimeira carta: " + cbr.primeiraCarta(state, 1));
        System.out.println("\tSegunda carta: " + cbr.segundaCarta(state, 2));
        System.out.println("\tTerceira carta: " + cbr.terceiraCarta(state, 3));
        
        System.out.println("Fim do teste");
    }
}
