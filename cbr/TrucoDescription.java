package trabalho.cbr;

import java.util.Arrays;
import java.util.List;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class TrucoDescription implements CaseComponent {
    String CASEID;
    Integer JogadorMao;
    Integer CartaAltaRobo;
    Integer CartaMediaRobo;
    Integer CartaBaixaRobo;
    Integer CartaAltaHumano;
    Integer CartaMediaHumano;
    Integer CartaBaixaHumano;
    Integer PrimeiraCartaRobo;
    Integer PrimeiraCartaHumano;
    Integer SegundaCartaRobo;
    Integer SegundaCartaHumano;
    Integer TerceiraCartaRobo;
    Integer TerceiraCartaHumano;
    Integer GanhadorPrimeiraRodada;
    Integer GanhadorSegundaRodada;
    Integer GanhadorTerceiraRodada;
    Integer RoboCartaVirada;
    Integer HumanoCartaVirada;
    Integer QuemPediuEnvido;
    Integer QuemPediuFaltaEnvido;
    Integer QuemPediuRealEnvido;
    Integer PontosEnvidoRobo;
    Integer PontosEnvidoHumano;
    Integer QuemNegouEnvido;
    Integer QuemGanhouEnvido;
    Integer TentosEnvido;
    Integer QuemFlor;
    Integer QuemContraFlor;
    Integer QuemContraFlorResto;
    Integer QuemNegouFlor;
    Integer PontosFlorRobo;
    Integer PontosFlorHumano;
    Integer QuemGanhouFlor;
    Integer TentosFlor;
    Integer QuemEscondeuPontosEnvido;
    Integer QuemEscondeuPontosFlor;
    Integer QuemTruco;
    Integer QuandoTruco;
    Integer QuemRetruco;
    Integer QuandoRetruco;
    Integer QuemValeQuatro;
    Integer QuandoValeQuatro;
    Integer QuemNegouTruco;
    Integer QuemGanhouTruco;
    Integer TentosTruco;
    Integer TentosAnterioresRobo;
    Integer TentosAnterioresHumano;
    Integer TentosPosterioresRobo;
    Integer TentosPosterioresHumano;
    Integer RoboMentiuEnvido;
    Integer HumanoMentiuEnvido;
    Integer RoboMentiuFlor;
    Integer HumanoMentiuFlor;
    Integer RoboMentiuTruco;
    Integer HumanoMentiuTruco;
    Integer QuemBaralho;

    public TrucoDescription(List<Integer> list) {
        // CASEID = Integer.toString(list.get(0));
        JogadorMao = list.get(1);
        CartaAltaRobo = list.get(2);
        CartaMediaRobo = list.get(3);
        CartaBaixaRobo = list.get(4);
        CartaAltaHumano = list.get(5);
        CartaMediaHumano = list.get(6);
        CartaBaixaHumano = list.get(7);
        PrimeiraCartaRobo = list.get(8);
        PrimeiraCartaHumano = list.get(9);
        SegundaCartaRobo = list.get(10);
        SegundaCartaHumano = list.get(11);
        TerceiraCartaRobo = list.get(12);
        TerceiraCartaHumano = list.get(13);
        GanhadorPrimeiraRodada = list.get(14);
        GanhadorSegundaRodada = list.get(15);
        GanhadorTerceiraRodada = list.get(16);
        RoboCartaVirada = list.get(17);
        HumanoCartaVirada = list.get(18);
        QuemPediuEnvido = list.get(19);
        QuemPediuFaltaEnvido = list.get(20);
        QuemPediuRealEnvido = list.get(21);
        PontosEnvidoRobo = list.get(22);
        PontosEnvidoHumano = list.get(23);
        QuemNegouEnvido = list.get(24);
        QuemGanhouEnvido = list.get(25);
        TentosEnvido = list.get(26);
        QuemFlor = list.get(27);
        QuemContraFlor = list.get(28);
        QuemContraFlorResto = list.get(29);
        QuemNegouFlor = list.get(30);
        PontosFlorRobo = list.get(31);
        PontosFlorHumano = list.get(32);
        QuemGanhouFlor = list.get(33);
        TentosFlor = list.get(34);
        QuemEscondeuPontosEnvido = list.get(35);
        QuemEscondeuPontosFlor = list.get(36);
        QuemTruco = list.get(37);
        QuandoTruco = list.get(38);
        QuemRetruco = list.get(39);
        QuandoRetruco = list.get(40);
        QuemValeQuatro = list.get(41);
        QuandoValeQuatro = list.get(42);
        QuemNegouTruco = list.get(43);
        QuemGanhouTruco = list.get(44);
        TentosTruco = list.get(45);
        TentosAnterioresRobo = list.get(46);
        TentosAnterioresHumano = list.get(47);
        TentosPosterioresRobo = list.get(48);
        TentosPosterioresHumano = list.get(49);
        RoboMentiuEnvido = list.get(50);
        HumanoMentiuEnvido = list.get(51);
        RoboMentiuFlor = list.get(52);
        HumanoMentiuFlor = list.get(53);
        RoboMentiuTruco = list.get(54);
        HumanoMentiuTruco = list.get(55);
        QuemBaralho = list.get(56);
    }

    public TrucoDescription() {
        JogadorMao = null;
        CartaAltaRobo = null;
        CartaMediaRobo = null;
        CartaBaixaRobo = null;
        CartaAltaHumano = null;
        CartaMediaHumano = null;
        CartaBaixaHumano = null;
        PrimeiraCartaRobo = null;
        PrimeiraCartaHumano = null;
        SegundaCartaRobo = null;
        SegundaCartaHumano = null;
        TerceiraCartaRobo = null;
        TerceiraCartaHumano = null;
        GanhadorPrimeiraRodada = null;
        GanhadorSegundaRodada = null;
        GanhadorTerceiraRodada = null;
        RoboCartaVirada = null;
        HumanoCartaVirada = null;
        QuemPediuEnvido = null;
        QuemPediuFaltaEnvido = null;
        QuemPediuRealEnvido = null;
        PontosEnvidoRobo = null;
        PontosEnvidoHumano = null;
        QuemNegouEnvido = null;
        QuemGanhouEnvido = null;
        TentosEnvido = null;
        QuemFlor = null;
        QuemContraFlor = null;
        QuemContraFlorResto = null;
        QuemNegouFlor = null;
        PontosFlorRobo = null;
        PontosFlorHumano = null;
        QuemGanhouFlor = null;
        TentosFlor = null;
        QuemEscondeuPontosEnvido = null;
        QuemEscondeuPontosFlor = null;
        QuemTruco = null;
        QuandoTruco = null;
        QuemRetruco = null;
        QuandoRetruco = null;
        QuemValeQuatro = null;
        QuandoValeQuatro = null;
        QuemNegouTruco = null;
        QuemGanhouTruco = null;
        TentosTruco = null;
        TentosAnterioresRobo = null;
        TentosAnterioresHumano = null;
        TentosPosterioresRobo = null;
        TentosPosterioresHumano = null;
        RoboMentiuEnvido = null;
        HumanoMentiuEnvido = null;
        RoboMentiuFlor = null;
        HumanoMentiuFlor = null;
        RoboMentiuTruco = null;
        HumanoMentiuTruco = null;
        QuemBaralho = null;
    }

    public TrucoDescription(TrucoDescription outro) {
        CASEID = outro.getCASEID();
        JogadorMao = outro.getJogadorMao();
        CartaAltaRobo = outro.getCartaAltaRobo();
        CartaMediaRobo = outro.getCartaMediaRobo();
        CartaBaixaRobo = outro.getCartaBaixaRobo();
        CartaAltaHumano = outro.getCartaAltaHumano();
        CartaMediaHumano = outro.getCartaMediaHumano();
        CartaBaixaHumano = outro.getCartaBaixaHumano();
        PrimeiraCartaRobo = outro.getPrimeiraCartaRobo();
        PrimeiraCartaHumano = outro.getPrimeiraCartaHumano();
        SegundaCartaRobo = outro.getSegundaCartaRobo();
        SegundaCartaHumano = outro.getSegundaCartaHumano();
        TerceiraCartaRobo = outro.getTerceiraCartaRobo();
        TerceiraCartaHumano = outro.getTerceiraCartaHumano();
        GanhadorPrimeiraRodada = outro.getGanhadorPrimeiraRodada();
        GanhadorSegundaRodada = outro.getGanhadorSegundaRodada();
        GanhadorTerceiraRodada = outro.getGanhadorTerceiraRodada();
        RoboCartaVirada = outro.getRoboCartaVirada();
        HumanoCartaVirada = outro.getHumanoCartaVirada();
        QuemPediuEnvido = outro.getQuemPediuEnvido();
        QuemPediuFaltaEnvido = outro.getQuemPediuFaltaEnvido();
        QuemPediuRealEnvido = outro.getQuemPediuRealEnvido();
        PontosEnvidoRobo = outro.getPontosEnvidoRobo();
        PontosEnvidoHumano = outro.getPontosEnvidoHumano();
        QuemNegouEnvido = outro.getQuemNegouEnvido();
        QuemGanhouEnvido = outro.getQuemGanhouEnvido();
        TentosEnvido = outro.getTentosEnvido();
        QuemFlor = outro.getQuemFlor();
        QuemContraFlor = outro.getQuemContraFlor();
        QuemContraFlorResto = outro.getQuemContraFlorResto();
        QuemNegouFlor = outro.getQuemNegouFlor();
        PontosFlorRobo = outro.getPontosFlorRobo();
        PontosFlorHumano = outro.getPontosFlorHumano();
        QuemGanhouFlor = outro.getQuemGanhouFlor();
        TentosFlor = outro.getTentosFlor();
        QuemEscondeuPontosEnvido = outro.getQuemEscondeuPontosEnvido();
        QuemEscondeuPontosFlor = outro.getQuemEscondeuPontosFlor();
        QuemTruco = outro.getQuemTruco();
        QuandoTruco = outro.getQuandoTruco();
        QuemRetruco = outro.getQuemRetruco();
        QuandoRetruco = outro.getQuandoRetruco();
        QuemValeQuatro = outro.getQuemValeQuatro();
        QuandoValeQuatro = outro.getQuandoValeQuatro();
        QuemNegouTruco = outro.getQuemNegouTruco();
        QuemGanhouTruco = outro.getQuemGanhouTruco();
        TentosTruco = outro.getTentosTruco();
        TentosAnterioresRobo = outro.getTentosAnterioresRobo();
        TentosAnterioresHumano = outro.getTentosAnterioresHumano();
        TentosPosterioresRobo = outro.getTentosPosterioresRobo();
        TentosPosterioresHumano = outro.getTentosPosterioresHumano();
        RoboMentiuEnvido = outro.getRoboMentiuEnvido();
        HumanoMentiuEnvido = outro.getHumanoMentiuEnvido();
        RoboMentiuFlor = outro.getRoboMentiuFlor();
        HumanoMentiuFlor = outro.getHumanoMentiuFlor();
        RoboMentiuTruco = outro.getRoboMentiuTruco();
        HumanoMentiuTruco = outro.getHumanoMentiuTruco();
        QuemBaralho = outro.getQuemBaralho();
    }
    
    public List<Integer> getAsList() {
        List<Integer> list = Arrays.asList(
                null,
                JogadorMao,
                CartaAltaRobo,
                CartaMediaRobo,
                CartaBaixaRobo,
                CartaAltaHumano,
                CartaMediaHumano,
                CartaBaixaHumano,
                PrimeiraCartaRobo,
                PrimeiraCartaHumano,
                SegundaCartaRobo,
                SegundaCartaHumano,
                TerceiraCartaRobo,
                TerceiraCartaHumano,
                GanhadorPrimeiraRodada,
                GanhadorSegundaRodada,
                GanhadorTerceiraRodada,
                RoboCartaVirada,
                HumanoCartaVirada,
                QuemPediuEnvido,
                QuemPediuFaltaEnvido,
                QuemPediuRealEnvido,
                PontosEnvidoRobo,
                PontosEnvidoHumano,
                QuemNegouEnvido,
                QuemGanhouEnvido,
                TentosEnvido,
                QuemFlor,
                QuemContraFlor,
                QuemContraFlorResto,
                QuemNegouFlor,
                PontosFlorRobo,
                PontosFlorHumano,
                QuemGanhouFlor,
                TentosFlor,
                QuemEscondeuPontosEnvido,
                QuemEscondeuPontosFlor,
                QuemTruco,
                QuandoTruco,
                QuemRetruco,
                QuandoRetruco,
                QuemValeQuatro,
                QuandoValeQuatro,
                QuemNegouTruco,
                QuemGanhouTruco,
                TentosTruco,
                TentosAnterioresRobo,
                TentosAnterioresHumano,
                TentosPosterioresRobo,
                TentosPosterioresHumano,
                RoboMentiuEnvido,
                HumanoMentiuEnvido,
                RoboMentiuFlor,
                HumanoMentiuFlor,
                RoboMentiuTruco,
                HumanoMentiuTruco,
                QuemBaralho
            );
        return list;
    }

    public String toString()
    {
        return "("+CASEID+"):"+JogadorMao+","+CartaAltaRobo+","+CartaMediaRobo+","+CartaBaixaRobo+","+CartaAltaHumano+","+CartaMediaHumano+","+CartaBaixaHumano+","+PrimeiraCartaRobo+","+PrimeiraCartaHumano+","+SegundaCartaRobo+","+SegundaCartaHumano+","+TerceiraCartaRobo+","+TerceiraCartaHumano+","+GanhadorPrimeiraRodada+","+GanhadorSegundaRodada+","+GanhadorTerceiraRodada+","+RoboCartaVirada+","+HumanoCartaVirada+","+QuemPediuEnvido+","+QuemPediuFaltaEnvido+","+QuemPediuRealEnvido+","+PontosEnvidoRobo+","+PontosEnvidoHumano+","+QuemNegouEnvido+","+QuemGanhouEnvido+","+TentosEnvido+","+QuemFlor+","+QuemContraFlor+","+QuemContraFlorResto+","+QuemNegouFlor+","+PontosFlorRobo+","+PontosFlorHumano+","+QuemGanhouFlor+","+TentosFlor+","+QuemEscondeuPontosEnvido+","+QuemEscondeuPontosFlor+","+QuemTruco+","+QuandoTruco+","+QuemRetruco+","+QuandoRetruco+","+QuemValeQuatro+","+QuandoValeQuatro+","+QuemNegouTruco+","+QuemGanhouTruco+","+TentosTruco+","+TentosAnterioresRobo+","+TentosAnterioresHumano+","+TentosPosterioresRobo+","+TentosPosterioresHumano+","+RoboMentiuEnvido+","+HumanoMentiuEnvido+","+RoboMentiuFlor+","+HumanoMentiuFlor+","+RoboMentiuTruco+","+HumanoMentiuTruco+","+QuemBaralho;
    }

    
    public boolean equals(Object o){
        if(! (o instanceof TrucoDescription)) return false;

        TrucoDescription other = (TrucoDescription) o;
        boolean the_same = true;
        List<Integer> this_list = this.getAsList();
        List<Integer> other_list = other.getAsList();
        for (int i = 0; i < this_list.size(); i++) {
            if (this_list.get(i) != other_list.get(i)) {
                the_same = false;
                break;
            }
        }
        return the_same;
    }

    @Override
    public Attribute getIdAttribute() {
        return new Attribute("CASEID", this.getClass());
    }


    public String getCASEID() {
        return CASEID;
    }


    public void setCASEID(String cASEID) {
        CASEID = cASEID;
    }


    public Integer getJogadorMao() {
        return JogadorMao;
    }


    public void setJogadorMao(Integer jogadorMao) {
        JogadorMao = jogadorMao;
    }


    public Integer getCartaAltaRobo() {
        return CartaAltaRobo;
    }


    public void setCartaAltaRobo(Integer cartaAltaRobo) {
        CartaAltaRobo = cartaAltaRobo;
    }


    public Integer getCartaMediaRobo() {
        return CartaMediaRobo;
    }


    public void setCartaMediaRobo(Integer cartaMediaRobo) {
        CartaMediaRobo = cartaMediaRobo;
    }


    public Integer getCartaBaixaRobo() {
        return CartaBaixaRobo;
    }


    public void setCartaBaixaRobo(Integer cartaBaixaRobo) {
        CartaBaixaRobo = cartaBaixaRobo;
    }


    public Integer getCartaAltaHumano() {
        return CartaAltaHumano;
    }


    public void setCartaAltaHumano(Integer cartaAltaHumano) {
        CartaAltaHumano = cartaAltaHumano;
    }


    public Integer getCartaMediaHumano() {
        return CartaMediaHumano;
    }


    public void setCartaMediaHumano(Integer cartaMediaHumano) {
        CartaMediaHumano = cartaMediaHumano;
    }


    public Integer getCartaBaixaHumano() {
        return CartaBaixaHumano;
    }


    public void setCartaBaixaHumano(Integer cartaBaixaHumano) {
        CartaBaixaHumano = cartaBaixaHumano;
    }


    public Integer getPrimeiraCartaRobo() {
        return PrimeiraCartaRobo;
    }


    public void setPrimeiraCartaRobo(Integer primeiraCartaRobo) {
        PrimeiraCartaRobo = primeiraCartaRobo;
    }


    public Integer getPrimeiraCartaHumano() {
        return PrimeiraCartaHumano;
    }


    public void setPrimeiraCartaHumano(Integer primeiraCartaHumano) {
        PrimeiraCartaHumano = primeiraCartaHumano;
    }


    public Integer getSegundaCartaRobo() {
        return SegundaCartaRobo;
    }


    public void setSegundaCartaRobo(Integer segundaCartaRobo) {
        SegundaCartaRobo = segundaCartaRobo;
    }


    public Integer getSegundaCartaHumano() {
        return SegundaCartaHumano;
    }


    public void setSegundaCartaHumano(Integer segundaCartaHumano) {
        SegundaCartaHumano = segundaCartaHumano;
    }


    public Integer getTerceiraCartaRobo() {
        return TerceiraCartaRobo;
    }


    public void setTerceiraCartaRobo(Integer terceiraCartaRobo) {
        TerceiraCartaRobo = terceiraCartaRobo;
    }


    public Integer getTerceiraCartaHumano() {
        return TerceiraCartaHumano;
    }


    public void setTerceiraCartaHumano(Integer terceiraCartaHumano) {
        TerceiraCartaHumano = terceiraCartaHumano;
    }


    public Integer getGanhadorPrimeiraRodada() {
        return GanhadorPrimeiraRodada;
    }


    public void setGanhadorPrimeiraRodada(Integer ganhadorPrimeiraRodada) {
        GanhadorPrimeiraRodada = ganhadorPrimeiraRodada;
    }


    public Integer getGanhadorSegundaRodada() {
        return GanhadorSegundaRodada;
    }


    public void setGanhadorSegundaRodada(Integer ganhadorSegundaRodada) {
        GanhadorSegundaRodada = ganhadorSegundaRodada;
    }


    public Integer getGanhadorTerceiraRodada() {
        return GanhadorTerceiraRodada;
    }


    public void setGanhadorTerceiraRodada(Integer ganhadorTerceiraRodada) {
        GanhadorTerceiraRodada = ganhadorTerceiraRodada;
    }


    public Integer getRoboCartaVirada() {
        return RoboCartaVirada;
    }


    public void setRoboCartaVirada(Integer roboCartaVirada) {
        RoboCartaVirada = roboCartaVirada;
    }


    public Integer getHumanoCartaVirada() {
        return HumanoCartaVirada;
    }


    public void setHumanoCartaVirada(Integer humanoCartaVirada) {
        HumanoCartaVirada = humanoCartaVirada;
    }


    public Integer getQuemPediuEnvido() {
        return QuemPediuEnvido;
    }


    public void setQuemPediuEnvido(Integer quemPediuEnvido) {
        QuemPediuEnvido = quemPediuEnvido;
    }


    public Integer getQuemPediuFaltaEnvido() {
        return QuemPediuFaltaEnvido;
    }


    public void setQuemPediuFaltaEnvido(Integer quemPediuFaltaEnvido) {
        QuemPediuFaltaEnvido = quemPediuFaltaEnvido;
    }


    public Integer getQuemPediuRealEnvido() {
        return QuemPediuRealEnvido;
    }


    public void setQuemPediuRealEnvido(Integer quemPediuRealEnvido) {
        QuemPediuRealEnvido = quemPediuRealEnvido;
    }


    public Integer getPontosEnvidoRobo() {
        return PontosEnvidoRobo;
    }


    public void setPontosEnvidoRobo(Integer pontosEnvidoRobo) {
        PontosEnvidoRobo = pontosEnvidoRobo;
    }


    public Integer getPontosEnvidoHumano() {
        return PontosEnvidoHumano;
    }


    public void setPontosEnvidoHumano(Integer pontosEnvidoHumano) {
        PontosEnvidoHumano = pontosEnvidoHumano;
    }


    public Integer getQuemNegouEnvido() {
        return QuemNegouEnvido;
    }


    public void setQuemNegouEnvido(Integer quemNegouEnvido) {
        QuemNegouEnvido = quemNegouEnvido;
    }


    public Integer getQuemGanhouEnvido() {
        return QuemGanhouEnvido;
    }


    public void setQuemGanhouEnvido(Integer quemGanhouEnvido) {
        QuemGanhouEnvido = quemGanhouEnvido;
    }


    public Integer getTentosEnvido() {
        return TentosEnvido;
    }


    public void setTentosEnvido(Integer tentosEnvido) {
        TentosEnvido = tentosEnvido;
    }


    public Integer getQuemFlor() {
        return QuemFlor;
    }


    public void setQuemFlor(Integer quemFlor) {
        QuemFlor = quemFlor;
    }


    public Integer getQuemContraFlor() {
        return QuemContraFlor;
    }


    public void setQuemContraFlor(Integer quemContraFlor) {
        QuemContraFlor = quemContraFlor;
    }


    public Integer getQuemContraFlorResto() {
        return QuemContraFlorResto;
    }


    public void setQuemContraFlorResto(Integer quemContraFlorResto) {
        QuemContraFlorResto = quemContraFlorResto;
    }


    public Integer getQuemNegouFlor() {
        return QuemNegouFlor;
    }


    public void setQuemNegouFlor(Integer quemNegouFlor) {
        QuemNegouFlor = quemNegouFlor;
    }


    public Integer getPontosFlorRobo() {
        return PontosFlorRobo;
    }


    public void setPontosFlorRobo(Integer pontosFlorRobo) {
        PontosFlorRobo = pontosFlorRobo;
    }


    public Integer getPontosFlorHumano() {
        return PontosFlorHumano;
    }


    public void setPontosFlorHumano(Integer pontosFlorHumano) {
        PontosFlorHumano = pontosFlorHumano;
    }


    public Integer getQuemGanhouFlor() {
        return QuemGanhouFlor;
    }


    public void setQuemGanhouFlor(Integer quemGanhouFlor) {
        QuemGanhouFlor = quemGanhouFlor;
    }


    public Integer getTentosFlor() {
        return TentosFlor;
    }


    public void setTentosFlor(Integer tentosFlor) {
        TentosFlor = tentosFlor;
    }


    public Integer getQuemEscondeuPontosEnvido() {
        return QuemEscondeuPontosEnvido;
    }


    public void setQuemEscondeuPontosEnvido(Integer quemEscondeuPontosEnvido) {
        QuemEscondeuPontosEnvido = quemEscondeuPontosEnvido;
    }


    public Integer getQuemEscondeuPontosFlor() {
        return QuemEscondeuPontosFlor;
    }


    public void setQuemEscondeuPontosFlor(Integer quemEscondeuPontosFlor) {
        QuemEscondeuPontosFlor = quemEscondeuPontosFlor;
    }


    public Integer getQuemTruco() {
        return QuemTruco;
    }


    public void setQuemTruco(Integer quemTruco) {
        QuemTruco = quemTruco;
    }


    public Integer getQuandoTruco() {
        return QuandoTruco;
    }


    public void setQuandoTruco(Integer quandoTruco) {
        QuandoTruco = quandoTruco;
    }


    public Integer getQuemRetruco() {
        return QuemRetruco;
    }


    public void setQuemRetruco(Integer quemRetruco) {
        QuemRetruco = quemRetruco;
    }


    public Integer getQuandoRetruco() {
        return QuandoRetruco;
    }


    public void setQuandoRetruco(Integer quandoRetruco) {
        QuandoRetruco = quandoRetruco;
    }


    public Integer getQuemValeQuatro() {
        return QuemValeQuatro;
    }


    public void setQuemValeQuatro(Integer quemValeQuatro) {
        QuemValeQuatro = quemValeQuatro;
    }


    public Integer getQuandoValeQuatro() {
        return QuandoValeQuatro;
    }


    public void setQuandoValeQuatro(Integer quandoValeQuatro) {
        QuandoValeQuatro = quandoValeQuatro;
    }


    public Integer getQuemNegouTruco() {
        return QuemNegouTruco;
    }


    public void setQuemNegouTruco(Integer quemNegouTruco) {
        QuemNegouTruco = quemNegouTruco;
    }


    public Integer getQuemGanhouTruco() {
        return QuemGanhouTruco;
    }


    public void setQuemGanhouTruco(Integer quemGanhouTruco) {
        QuemGanhouTruco = quemGanhouTruco;
    }


    public Integer getTentosTruco() {
        return TentosTruco;
    }


    public void setTentosTruco(Integer tentosTruco) {
        TentosTruco = tentosTruco;
    }


    public Integer getTentosAnterioresRobo() {
        return TentosAnterioresRobo;
    }


    public void setTentosAnterioresRobo(Integer tentosAnterioresRobo) {
        TentosAnterioresRobo = tentosAnterioresRobo;
    }


    public Integer getTentosAnterioresHumano() {
        return TentosAnterioresHumano;
    }


    public void setTentosAnterioresHumano(Integer tentosAnterioresHumano) {
        TentosAnterioresHumano = tentosAnterioresHumano;
    }


    public Integer getTentosPosterioresRobo() {
        return TentosPosterioresRobo;
    }


    public void setTentosPosterioresRobo(Integer tentosPosterioresRobo) {
        TentosPosterioresRobo = tentosPosterioresRobo;
    }


    public Integer getTentosPosterioresHumano() {
        return TentosPosterioresHumano;
    }


    public void setTentosPosterioresHumano(Integer tentosPosterioresHumano) {
        TentosPosterioresHumano = tentosPosterioresHumano;
    }


    public Integer getRoboMentiuEnvido() {
        return RoboMentiuEnvido;
    }


    public void setRoboMentiuEnvido(Integer roboMentiuEnvido) {
        RoboMentiuEnvido = roboMentiuEnvido;
    }


    public Integer getHumanoMentiuEnvido() {
        return HumanoMentiuEnvido;
    }


    public void setHumanoMentiuEnvido(Integer humanoMentiuEnvido) {
        HumanoMentiuEnvido = humanoMentiuEnvido;
    }


    public Integer getRoboMentiuFlor() {
        return RoboMentiuFlor;
    }


    public void setRoboMentiuFlor(Integer roboMentiuFlor) {
        RoboMentiuFlor = roboMentiuFlor;
    }


    public Integer getHumanoMentiuFlor() {
        return HumanoMentiuFlor;
    }


    public void setHumanoMentiuFlor(Integer humanoMentiuFlor) {
        HumanoMentiuFlor = humanoMentiuFlor;
    }


    public Integer getRoboMentiuTruco() {
        return RoboMentiuTruco;
    }


    public void setRoboMentiuTruco(Integer roboMentiuTruco) {
        RoboMentiuTruco = roboMentiuTruco;
    }


    public Integer getHumanoMentiuTruco() {
        return HumanoMentiuTruco;
    }


    public void setHumanoMentiuTruco(Integer humanoMentiuTruco) {
        HumanoMentiuTruco = humanoMentiuTruco;
    }


    public Integer getQuemBaralho() {
        return QuemBaralho;
    }


    public void setQuemBaralho(Integer quemBaralho) {
        QuemBaralho = quemBaralho;
    }

}