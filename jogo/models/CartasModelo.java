package trabalho.jogo.models;

public class CartasModelo implements Comparable<CartasModelo>{
	private String carta;
	private int valorImportancia;
	private String naipe;
	private int valorQueContaParaOenvido;
	private int id;
	
	
	public CartasModelo(String carta, int valorImportancia, String naipe, int valorQueContaParaOenvido, int id) {
		super();
		this.carta = carta;
		this.valorImportancia = valorImportancia;
		this.naipe = naipe;
		this.valorQueContaParaOenvido = valorQueContaParaOenvido;
		this.id = id;
	}
	public String getCarta() {
		return carta;
	}
	public void setCarta(String carta) {
		this.carta = carta;
	}
	public int getValorImportancia() {
		return valorImportancia;
	}
	public void setValorImportancia(int valorImportancia) {
		this.valorImportancia = valorImportancia;
	}
	public String getNaipe() {
		return naipe;
	}
	public void setNaipe(String naipe) {
		this.naipe = naipe;
	}
	public int getValorQueContaParaOenvido() {
		return valorQueContaParaOenvido;
	}
	public void setValorQueContaParaOenvido(int valorQueContaParaOenvido) {
		this.valorQueContaParaOenvido = valorQueContaParaOenvido;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	@Override
	public int compareTo(CartasModelo outraCarta) {
		if(this.id > outraCarta.id) return -1;
		if(this.id < outraCarta.id) return 1;
		return 0;
	}
	

}
