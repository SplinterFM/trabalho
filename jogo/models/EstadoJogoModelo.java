package trabalho.jogo.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import trabalho.jogo.jogoNegocio.ControlaPartidaNegocio;

public class EstadoJogoModelo {

	private int quemJogaAgora;
	private int emQueRodadaEsta;
	private int pontuacaoRobo;
	private int pontuacaoHumano;
	private int qualEhAmao;
	private boolean roboTemFlor;
	private boolean humanoTemFlor;

	// cada mão tem duas "jogadas"
	private int contadorJogadas;

	// listas jogadas
	private List<String> listaJogadasQuePodemSerChamadas = new ArrayList<String>();
	private List<JogadasChamadasModelo> listaJogadasChamadas = new ArrayList<JogadasChamadasModelo>();
	private List<JogadasQuePodemSerAceitasModelo> listaJogadasQuePodemSerAceitas = new ArrayList<JogadasQuePodemSerAceitasModelo>();
	private List<JogadasAceitasModelo> listaJogadasAceitas = new ArrayList<JogadasAceitasModelo>();
	// listas pontos
	private List<String> listaPontosQuePodemSerChamados = new ArrayList<String>();
	private List<JogadasChamadasModelo> listaPontosChamados = new ArrayList<JogadasChamadasModelo>();
	private List<JogadasQuePodemSerAceitasModelo> listaPontosQuePodemSerAceitos = new ArrayList<JogadasQuePodemSerAceitasModelo>();
	private List<JogadasAceitasModelo> listaPontosAceitos = new ArrayList<JogadasAceitasModelo>();

	public int getQuemJogaAgora() {
		return quemJogaAgora;
	}

	public void setQuemJogaAgora(int quemJogaAgora) {
		this.quemJogaAgora = quemJogaAgora;
	}

	public int getEmQueRodadaEsta() {
		return emQueRodadaEsta;
	}

	public void setEmQueRodadaEsta(int emQueRodadaEsta) {
		this.emQueRodadaEsta = emQueRodadaEsta;
	}

	public int getPontuacaoRobo() {
		return pontuacaoRobo;
	}

	public void setPontuacaoRobo(int pontuacaoRobo) {
		this.pontuacaoRobo = pontuacaoRobo;
	}

	public boolean isRoboTemFlor() {
		return roboTemFlor;
	}

	public void setRoboTemFlor(boolean roboTemFlor) {
		this.roboTemFlor = roboTemFlor;
	}

	public boolean isHumanoTemFlor() {
		return humanoTemFlor;
	}

	public void setHumanoTemFlor(boolean humanoTemFlor) {
		this.humanoTemFlor = humanoTemFlor;
	}

	public int getPontuacaoHumano() {
		return pontuacaoHumano;
	}

	public void setPontuacaoHumano(int pontuacaoHumano) {
		this.pontuacaoHumano = pontuacaoHumano;
	}

	public int getQualEhAmao() {
		return qualEhAmao;
	}

	public void setQualEhAmao(int qualEhAmao) {
		this.qualEhAmao = qualEhAmao;
	}

	public boolean isTemMaisRodada() {
		return (pontuacaoRobo < 24 && pontuacaoHumano < 24 ? true : false);
	}

	private int retornaRodadaDaMao() {
		if (contadorJogadas == 0 || contadorJogadas == 1 || contadorJogadas == 2)
			return 1;
		if (contadorJogadas == 3 || contadorJogadas == 4)
			return 2;
		if (contadorJogadas == 3 || contadorJogadas == 4)
			return 3;
		return 0;
	}

	public List<String> listaJogadasQuePodemSerChamadas() {
		if (listaJogadasChamadas.isEmpty() && listaJogadasQuePodemSerAceitas.isEmpty()) {
			listaJogadasQuePodemSerChamadas.add("Truco");
			listaJogadasQuePodemSerChamadas.add("Ir ao baralho");

		} else if (!listaJogadasChamadas.isEmpty() && qualEhAmao == 1) {// verificar qual foi a última jogada que foi

			removeJogadasJaChamadas();
			verificaSeJogadaJaFoiChamadaEadicionaProxima();

		} else if (qualEhAmao != 1) {
			removeJogadasJaChamadas();
			verificaSeJogadaJaFoiChamadaEadicionaProxima();

		}

		return listaJogadasQuePodemSerChamadas;

	}

	public List<String> listaPontosQuePodemSerChamados() {
		if (listaPontosChamados.isEmpty() && listaPontosQuePodemSerChamados.isEmpty() && retornaRodadaDaMao() == 1
				&& humanoTemFlor) {
			// não precisa verificar se rôbo tem flor porque é verificado no
			// controlaRodadaNegocio
			listaPontosQuePodemSerChamados.add("Flor");

			listaPontosQuePodemSerChamados.add("Envido");

			listaPontosQuePodemSerChamados.add("RealEnvido");

		} else if (listaPontosChamados.isEmpty() && listaPontosQuePodemSerChamados.isEmpty()
				&& retornaRodadaDaMao() == 1) {
			listaPontosQuePodemSerChamados.add("Envido");

			listaPontosQuePodemSerChamados.add("RealEnvido");

		}

		else if (!listaPontosChamados.isEmpty() && qualEhAmao == 1 && listaPontosQuePodemSerChamados.isEmpty()) {// verificar
																													// qual
																													// foi
																													// a
																													// última
																													// jogada
																													// //
																													// que
																													// foi
			listaPontosQuePodemSerChamados.add("Envido");
			if (humanoTemFlor)
				listaPontosQuePodemSerChamados.add("Flor");
			listaPontosQuePodemSerChamados.add("RealEnvido"); // chamada

			verificaSePontoJaFoiChamadoEadicionaProximo();
			removePontosJaChamados();

		} else if (qualEhAmao == 2 || qualEhAmao == 3) {

			removePontosQueSoPodemSerChamadosNaPrimeiraRodadaDaMao();
		}
		return listaPontosQuePodemSerChamados;

	}

	private void removePontosQueSoPodemSerChamadosNaPrimeiraRodadaDaMao() {
		for (int i = 0; i < listaPontosQuePodemSerChamados.size(); i++) {
			if (listaPontosQuePodemSerChamados.get(i).equals("Envido"))
				listaPontosQuePodemSerChamados.remove(i);
			if (listaPontosQuePodemSerChamados.get(i).equals("RealEnvido"))
				listaPontosQuePodemSerChamados.remove(i);
			if (listaPontosQuePodemSerChamados.get(i).equals("Flor"))
				listaPontosQuePodemSerChamados.remove(i);
		}
	}

	private void verificaSeJogadaJaFoiChamadaEadicionaProxima() {
		for (int i = 0; i < listaJogadasChamadas.size(); i++) {
			// verifica se foi chamado Truco e disponibiliza o Retruco para ser chamado.
			if (listaJogadasChamadas.get(i).getJogadaChamada().contains("Truco")
					&& !listaJogadasChamadas.get(i).getJogadaChamada().contains("ReTruco"))
				listaJogadasQuePodemSerChamadas.add("ReTruco");
			// verifica se foi chamado ReTruco e disponibiliza o ValeQuatro para ser
			// chamado.
			if (listaJogadasChamadas.get(i).getJogadaChamada().contains("ReTruco")
					&& !listaJogadasChamadas.get(i).getJogadaChamada().contains("ValeQuatro"))
				listaJogadasQuePodemSerChamadas.add("ValeQuatro");

		}
	}

	private void verificaSePontoJaFoiChamadoEadicionaProximo() {
		for (int i = 0; i < listaPontosChamados.size(); i++) {
			// verifica se foi chamado envido ou real envido e disponibiliza o falta envido.
			if (listaPontosChamados.get(i).getJogadaChamada().contains("Envido")
					|| listaPontosChamados.get(i).getJogadaChamada().contains("RealEnvido")
							&& !listaPontosChamados.get(i).getJogadaChamada().contains("FaltaEnvido")
							&& !pontoEstaNaListaParaSerAceito("FaltaEnvido"))
				listaPontosQuePodemSerChamados.add("FaltaEnvido");

		}
	}

	private void removeJogadasJaChamadas() {
		for (JogadasChamadasModelo jogadasChamadas : listaJogadasChamadas) {
			for (int i = 0; i < listaJogadasQuePodemSerAceitas.size(); i++) {
				if (jogadasChamadas.getJogadaChamada().equalsIgnoreCase(listaJogadasQuePodemSerChamadas.get(i)))
					listaJogadasQuePodemSerChamadas.remove(i);
			}

		}
	}

	private boolean pontoEstaNaListaParaSerAceito(String pontoQueVaiSerAdicionado) {
		boolean jaExiste = false;
		for (JogadasQuePodemSerAceitasModelo ponto : listaPontosQuePodemSerAceitos) {
			if (ponto.getJogadaQuePodeSerAceita().equalsIgnoreCase(pontoQueVaiSerAdicionado))
				;
			jaExiste = true;
		}
		return jaExiste;
	}

	private void removePontosJaChamados() {
		for (JogadasChamadasModelo pontosChamados : listaPontosChamados) {
			for (int i = 0; i < listaPontosQuePodemSerChamados.size(); i++) {
				if (pontosChamados.getJogadaChamada().equalsIgnoreCase(listaPontosQuePodemSerChamados.get(i)))
					listaPontosQuePodemSerChamados.remove(i);
				// se flor for chamado remove pontos
				if (pontosChamados.getJogadaChamada().equalsIgnoreCase("flor")) {
					listaPontosQuePodemSerChamados.remove("Envido");
					listaPontosQuePodemSerChamados.remove("RealEnvido");
					listaPontosQuePodemSerChamados.remove("FaltaEnvido");

				}
			}

		}
	}

	public int getContadorJogadas() {
		return contadorJogadas;
	}

	public void setContadorJogadas(int contadorJogadas) {
		this.contadorJogadas = contadorJogadas;
	}

	public List<JogadasChamadasModelo> getListaJogadasChamadas() {
		return listaJogadasChamadas;
	}

	public void setListaJogadasChamadas(List<JogadasChamadasModelo> listaJogadasChamadasModelo) {
		this.listaJogadasChamadas = listaJogadasChamadasModelo;
	}

	// retorna as jogadas que podem ser aceitas por jogador
	public List<String> getListaJogadasQuePodemSerAceitas(int jogador) {
		List<String> listaJogadasQuePodemSerAceitasPorJogador = new ArrayList<String>();
		for (int i = 0; i < listaJogadasQuePodemSerAceitas.size(); i++) {
			if (listaJogadasQuePodemSerAceitas.get(i).getQuemPodeAceitar() == jogador)
				listaJogadasQuePodemSerAceitasPorJogador
						.add(listaJogadasQuePodemSerAceitas.get(i).getJogadaQuePodeSerAceita());
		}
		if (listaJogadasQuePodemSerAceitasPorJogador.isEmpty())
			listaJogadasQuePodemSerAceitasPorJogador.add("Não existem jogadas para serem aceitas no momento.");

		if (!listaJogadasQuePodemSerAceitasPorJogador.isEmpty())
			listaJogadasQuePodemSerAceitasPorJogador.add("Não Aceitar");

		return listaJogadasQuePodemSerAceitasPorJogador;
	}

	private void preencheListaPontosQuePodemSerAceitosPorJogador(int jogadorQuePodeAceitar) {
		int jogadorQueChamou = 0;
		// verifica quem que chamou com base de quem tem que aceitar
		if (jogadorQuePodeAceitar == 1)
			jogadorQueChamou = 2;
		if (jogadorQuePodeAceitar == 2)
			jogadorQueChamou = 1;
		// pega o ultimo ponto chamado e adiciona na lista
		JogadasChamadasModelo pontoQueFoiChamado = listaPontosChamados.get(listaPontosChamados.size() - 1);
		// verifica se quem chamou foi o outro jogador e habilita
		if (pontoQueFoiChamado.getQuemChamou() == jogadorQueChamou) {
			JogadasQuePodemSerAceitasModelo pontosQuePodemSerAceitos = new JogadasQuePodemSerAceitasModelo();
			pontosQuePodemSerAceitos.setQuemPodeAceitar(jogadorQuePodeAceitar);
			pontosQuePodemSerAceitos.setJogadaQuePodeSerAceita(pontoQueFoiChamado.getJogadaChamada());
			listaPontosQuePodemSerAceitos.add(pontosQuePodemSerAceitos);
		}

	}

	// retorna pontos que podem ser aceitos por jogador
	public List<String> getListaPontosQuePodemSerAceitos(int jogador) {
		preencheListaPontosQuePodemSerAceitosPorJogador(jogador);
		List<String> listaPontosQuePodemSerAceitosPorJogador = new ArrayList<String>();
		for (int i = 0; i < listaPontosQuePodemSerAceitos.size(); i++) {
			if (listaPontosQuePodemSerAceitos.get(i).getQuemPodeAceitar() == jogador)
				listaPontosQuePodemSerAceitosPorJogador
						.add(listaPontosQuePodemSerAceitos.get(i).getJogadaQuePodeSerAceita());
		}
		if (listaPontosQuePodemSerAceitosPorJogador.isEmpty())
			listaPontosQuePodemSerAceitosPorJogador.add("Não existem pontos para serem aceitas no momento.");

		if (!listaPontosQuePodemSerAceitosPorJogador.isEmpty())
			listaPontosQuePodemSerAceitosPorJogador.add("Não Aceitar");
		return listaPontosQuePodemSerAceitosPorJogador;
	}

	public List<JogadasQuePodemSerAceitasModelo> getListaPontosQuePodemSerAceitos() {
		return listaPontosQuePodemSerAceitos;
	}

	public void setListaPontosQuePodemSerAceitos(List<JogadasQuePodemSerAceitasModelo> listaPontosQuePodemSerAceitos) {
		this.listaPontosQuePodemSerAceitos = listaPontosQuePodemSerAceitos;
	}

	public void setListaJogadasQuePodemSerAceitas(
			List<JogadasQuePodemSerAceitasModelo> listaJogadasQuePodemSerAceitas) {
		this.listaJogadasQuePodemSerAceitas = listaJogadasQuePodemSerAceitas;
	}

	public List<JogadasAceitasModelo> getListaPontosAceitosModelo() {
		return listaPontosAceitos;
	}

	public void setListaPontosAceitosModelo(List<JogadasAceitasModelo> listaPontosAceitosModelo) {
		this.listaPontosAceitos = listaPontosAceitosModelo;
	}

	public void setListaPontosChamados(List<JogadasChamadasModelo> listaPontosChamados) {
		this.listaPontosChamados = listaPontosChamados;
	}

	public List<JogadasAceitasModelo> getListaJogadasAceitas() {
		return listaJogadasAceitas;
	}

	public void setListaJogadasAceitas(List<JogadasAceitasModelo> listaJogadasAceitas) {
		this.listaJogadasAceitas = listaJogadasAceitas;
	}

	public List<JogadasAceitasModelo> getListaPontosAceitos() {
		return listaPontosAceitos;
	}

	public void setListaPontosAceitos(List<JogadasAceitasModelo> listaPontosAceitos) {
		this.listaPontosAceitos = listaPontosAceitos;
	}

}
