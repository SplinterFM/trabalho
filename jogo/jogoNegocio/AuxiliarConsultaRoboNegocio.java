package trabalho.jogo.jogoNegocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import trabalho.jogo.models.CartaPorImportanciaModelo;
import trabalho.jogo.models.CartasModelo;

public class AuxiliarConsultaRoboNegocio {

	public List<CartaPorImportanciaModelo> retornaListaCartaPorImportancia(List<CartasModelo> listaCartasJogador) {
		List<CartaPorImportanciaModelo> listaCartasPorImportancia = new ArrayList<CartaPorImportanciaModelo>();
		// ordena por Id, visto que a classe carta modelo implementa a interface
		// comparable
		Collections.sort(listaCartasJogador);

		for (CartasModelo cartaJogador : listaCartasJogador) {
			CartaPorImportanciaModelo cartaImportancia = new CartaPorImportanciaModelo(cartaJogador.getId(),
					cartaJogador.getValorImportancia(), cartaJogador.getCarta());
			listaCartasPorImportancia.add(cartaImportancia);
		}

		return listaCartasPorImportancia;

	}

}
