package trabalho.jogo.jogoNegocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import trabalho.jogo.models.CartasModelo;

public class DarCartasNegocio {
	List<CartasModelo> cartas;
	
	
	public DarCartasNegocio() {
		adicionarTodasCartas();
	}
	
	private void adicionarTodasCartas() {
		cartas = new ArrayList<CartasModelo>();
		cartas.add(new CartasModelo("4c", 0, "c", 4, 00));
		cartas.add(new CartasModelo("4p", 0, "p", 4, 01));
		cartas.add(new CartasModelo("4o", 0, "o", 4, 02));
		cartas.add(new CartasModelo("4e", 0, "e", 4, 03));
		cartas.add(new CartasModelo("5c", 1, "c", 5, 10));
		cartas.add(new CartasModelo("5p", 1, "p", 5, 11));
		cartas.add(new CartasModelo("5o", 1, "o", 5, 12));
		cartas.add(new CartasModelo("5e", 1, "e", 5, 13));
		cartas.add(new CartasModelo("6c", 2, "c", 6, 20));
		cartas.add(new CartasModelo("6p", 2, "p", 6, 21));
		cartas.add(new CartasModelo("6o", 2, "o", 6, 22));
		cartas.add(new CartasModelo("6e", 2, "e", 6, 23));
		cartas.add(new CartasModelo("7c", 3, "c", 7, 30));
		cartas.add(new CartasModelo("7p", 3, "p", 7, 31));
		cartas.add(new CartasModelo("10c", 4, "c", 0, 40));
		cartas.add(new CartasModelo("10p", 4, "p", 0, 41));
		cartas.add(new CartasModelo("10o", 4, "o", 0, 42));
		cartas.add(new CartasModelo("10e", 4, "e", 0, 43));
		cartas.add(new CartasModelo("11c", 5, "c", 0, 50));
		cartas.add(new CartasModelo("11p", 5, "p", 0, 51));
		cartas.add(new CartasModelo("11o", 5, "o", 0, 52));
		cartas.add(new CartasModelo("11e", 5, "e", 0, 53));
		cartas.add(new CartasModelo("12c", 6, "c", 0, 60));
		cartas.add(new CartasModelo("12p", 6, "p", 0, 61));
		cartas.add(new CartasModelo("12o", 6, "o", 0, 62));
		cartas.add(new CartasModelo("12e", 6, "e", 0, 63));
		cartas.add(new CartasModelo("1c", 7, "c", 1, 70));
		cartas.add(new CartasModelo("1o", 7, "o", 1, 71));
		cartas.add(new CartasModelo("2c", 8, "c", 2, 80));
		cartas.add(new CartasModelo("2p", 8, "p", 2, 81));
		cartas.add(new CartasModelo("2o", 8, "o", 2, 82));
		cartas.add(new CartasModelo("2e", 8, "e", 2, 83));
		cartas.add(new CartasModelo("3c", 9, "c", 3, 90));
		cartas.add(new CartasModelo("3p", 9, "p", 3, 91));
		cartas.add(new CartasModelo("3o", 9, "o", 3, 92));
		cartas.add(new CartasModelo("3e", 9, "e", 3, 93));
		cartas.add(new CartasModelo("7o", 10, "o", 7, 100));
		cartas.add(new CartasModelo("7e", 11, "e", 7, 110));
		cartas.add(new CartasModelo("1p", 12, "p", 1, 120));
		cartas.add(new CartasModelo("1e", 13, "e", 1, 130));
	}
	
	
	public CartasModelo entregarCartas() {
	   Collections.shuffle(cartas);
	   CartasModelo retorno = cartas.get(0);
	   for(int i=0; i< cartas.size(); i++) {
		   if(cartas.get(i).getCarta().equalsIgnoreCase(retorno.getCarta())) cartas.remove(i);
	   }
	    
	   return retorno;
	   
	}
	
	
	

}
