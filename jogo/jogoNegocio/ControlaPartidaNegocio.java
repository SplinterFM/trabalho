package trabalho.jogo.jogoNegocio;


public class ControlaPartidaNegocio {
	//classe padrão singleton
	public static ControlaPartidaNegocio controlaPartidaNegocio = null;

	private ControlaPartidaNegocio() {
		
	}
	public static ControlaPartidaNegocio getInstacia() {
		if (controlaPartidaNegocio == null) controlaPartidaNegocio = new ControlaPartidaNegocio();	
		return controlaPartidaNegocio;
	}
	
	//atributos classe
	private int contadorRodadas = 0;
	private int pontosHumano=0;
	private int pontosRobo =0;
	private int pontosAnterioresRobo=0;
	private int pontosAnterioresHumano=0;
	
	
	
	
	public int getPontosAnterioresRobo() {
		return pontosAnterioresRobo;
	}
	public void setPontosAnterioresRobo(int pontosAnterioresRobo) {
		this.pontosAnterioresRobo = pontosAnterioresRobo;
	}
	public int getPontosAnterioresHumano() {
		return pontosAnterioresHumano;
	}
	public void setPontosAnterioresHumano(int pontosAnterioresHumano) {
		this.pontosAnterioresHumano = pontosAnterioresHumano;
	}
	public int getContadorRodadas() {
		return contadorRodadas;
	}
	
	public void setContadorRodadas(int contadorRodadas) {
		this.contadorRodadas = contadorRodadas;
	}
	public int getPontosHumano() {
		return pontosHumano;
	}
	public void setPontosHumano(int pontosHumano) {
		this.pontosHumano = pontosHumano;
	}
	public int getPontosRobo() {
		return pontosRobo;
	}
	public void setPontosRobo(int pontosRobo) {
		this.pontosRobo = pontosRobo;
	}
	
	
	

}
