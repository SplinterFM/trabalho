package trabalho.jogo.jogoNegocio;

import java.util.ArrayList;
import java.util.List;

import trabalho.cbr.TrucoCBR;
import trabalho.cbr.TrucoDescription;
import trabalho.jogo.models.CartaPorImportanciaModelo;
import trabalho.jogo.models.CartasModelo;
import trabalho.jogo.models.EstadoJogoModelo;
import trabalho.jogo.models.JogadasAceitasModelo;
import trabalho.jogo.models.JogadasChamadasModelo;
import trabalho.jogo.models.JogadasNaoAceitasModelo;
import trabalho.jogo.models.JogadasQuePodemSerAceitasModelo;
import trabalho.jogo.models.QuemGanhouPontosModelo;
import trabalho.jogo.models.QuemGanhouRodadaModelo;

public class ControlaRodadaNegocio {
	TrucoCBR trucoCbr = new TrucoCBR();

	// instancia partida
	ControlaPartidaNegocio controlaPartidaNegocio = ControlaPartidaNegocio.getInstacia();
	// atributos necessários para controle da rodada
	DarCartasNegocio darCartasNegocio = new DarCartasNegocio();
	private int contadorMao = 0;
	public int ultimoAjogar = 0;
	// esse atributo receberá as jogadas individuais, uma mão é composta por 6
	// "jogadas"
	private int contadorJogadas = 0;

	// listas cartas
	private List<CartasModelo> listaCartasRecebidasRobo = new ArrayList<CartasModelo>();
	private List<CartasModelo> listaCartasRecebidasHumano = new ArrayList<CartasModelo>();
	private List<CartasModelo> listaCartas = new ArrayList<CartasModelo>();
	// aqui são As cartas que tem na mão removendoas já jogadas
	private List<CartasModelo> listaCartasHumanoTemNaMao = new ArrayList<CartasModelo>();
	private List<CartasModelo> listaCartasRoboTemNaMao = new ArrayList<CartasModelo>();

	// lista cartas Jogadas
	private List<String> listaCartasJogadasRobo = new ArrayList<String>();
	private List<String> listaCartasJogadasHumano = new ArrayList<String>();
	// jogadas chamadas para passar para o estadoJogoModelo, as aceitas terão a
	// lógica na classe EstadoJogoModelo
	private List<JogadasChamadasModelo> listaJogadasChamadas = new ArrayList<JogadasChamadasModelo>();
	private List<JogadasAceitasModelo> listaJogadasAceitas = new ArrayList<JogadasAceitasModelo>();
	private List<JogadasNaoAceitasModelo> listaJogadasNaoAceitas = new ArrayList<JogadasNaoAceitasModelo>();
	// Pontos chamados para passar para o estadoJogoModelo, os aceitos terão a
	// lógica na classe EstadoJogoModelo
	private List<JogadasChamadasModelo> listaPontosChamados = new ArrayList<JogadasChamadasModelo>();
	private List<JogadasAceitasModelo> listaPontosAceitos = new ArrayList<JogadasAceitasModelo>();
	private List<JogadasNaoAceitasModelo> listPontosNaoAceitos = new ArrayList<JogadasNaoAceitasModelo>();

	private boolean pontosJaChamados = false;

	public void setPontosJaChamados(boolean jaFoiChamado) {
		pontosJaChamados = jaFoiChamado;
	}

	public boolean getPodeChamarPontosAinda() {
		if (pontosJaChamados == false)
			return true;
		else
			return false;
	}

	public int getContadorJogadas() {
		return contadorJogadas;
	}

	public int getPontosHumano() {
		return controlaPartidaNegocio.getPontosHumano();
	}

	public int getPontosRobo() {
		return controlaPartidaNegocio.getPontosRobo();
	}

	public List<CartasModelo> darCartasRobo() {
		// adiciona as cartas como objeto
		if (listaCartas.isEmpty())
			darCartas();

		for (int i = 0; i < 3; i++) {
			listaCartasRecebidasRobo.add(listaCartas.get(i));
			System.out.println("carta "+ i+1+" rôbo: "+ listaCartas.get(i).getCarta());

		}
		if (listaCartasRoboTemNaMao.isEmpty())
			listaCartasRoboTemNaMao = new ArrayList<CartasModelo>(listaCartasRecebidasRobo);

		return listaCartasRoboTemNaMao;
	}

	/*
	 * public List<CartasModelo> darCartasRobo() { // adiciona as cartas como objeto
	 * if (listaCartas.isEmpty()) darCartas();
	 * 
	 * for (int i = 0; i < 3; i++) {
	 * listaCartasRecebidasRobo.add(listaCartas.get(i)); } if
	 * (listaCartasRoboTemNaMao.isEmpty()) listaCartasRoboTemNaMao =
	 * listaCartasRecebidasRobo;
	 * 
	 * return listaCartasRecebidasRobo; }
	 */

	// se for zero os dois podem chamar
	public int quemPodeChamarJogoAgora() {
		String ultimaJogadaChamada;
		if (listaJogadasChamadas.isEmpty())
			return 0;
		if (!listaJogadasChamadas.isEmpty()
				&& listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getQuemChamou() == 2)
			return 1;
		if (!listaJogadasChamadas.isEmpty()
				&& listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getQuemChamou() == 1)
			return 2;
		return 0;

	}

	public List<String> darCartasHumano() {
		// passa Apenas a String das cartas para a interface
		List<String> listaCartasHumanoInterface = new ArrayList<String>();

		if (listaCartas.isEmpty())
			darCartas();
		for (int i = 3; i < listaCartas.size(); i++) {
			listaCartasRecebidasHumano.add(listaCartas.get(i));
		}
		if (listaCartasHumanoTemNaMao.isEmpty())
			listaCartasHumanoTemNaMao = new ArrayList<>(listaCartasRecebidasHumano);

		// adiciona as Strings das Cartas HUmâno para mostrar as Strings
		for (CartasModelo carta : listaCartasHumanoTemNaMao) {
			listaCartasHumanoInterface.add(carta.getCarta());
		}

		return listaCartasHumanoInterface;
	}

	public int quemEhMao() {
		// numeros pares o jogador que é mão é o humano números impares o mão é o robô
		return (ehPar() ? 2 : 1);
	}

	private List<CartasModelo> darCartas() {
		// adiciona 6 cartas e retorna, depois na interface retorna 3 p/ cada jogador
		for (int i = 0; i < 6; i++) {
			listaCartas.add(darCartasNegocio.entregarCartas());
		}
		if (contadorMao == 0 || contadorMao <= 3) {
			contadorMao++;
		} else {
			contadorMao = 0;
			controlaPartidaNegocio.setContadorRodadas(controlaPartidaNegocio.getContadorRodadas() + 1);
		}

		controlaPartidaNegocio.setContadorRodadas(controlaPartidaNegocio.getContadorRodadas() + 1);
		return listaCartas;
	}

	private boolean ehPar() {
		return (controlaPartidaNegocio.getContadorRodadas() % 2 == 0);
	}

	public int quemJogaAgora() {
		int quemJoga = 0;
		if (ultimoAjogar == 0) {
			quemJoga = quemEhMao();
			armazenaTentosInicioDaRodada();

		} else if (listaCartasJogadasHumano.size() == 1 && listaCartasJogadasRobo.isEmpty()) {
			quemJoga = 1;

		} else if (listaCartasJogadasHumano.isEmpty() && listaCartasJogadasRobo.size() == 1) {
			quemJoga = 2;
		}
		// mão 2
		if (listaCartasJogadasHumano.size() == 1 && listaCartasJogadasRobo.size() == 1
				&& quemGanhouAPrimeiraMao() == 1) {
			quemJoga = 1;

		} else if (listaCartasJogadasHumano.size() == 1 && listaCartasJogadasRobo.size() == 1
				&& quemGanhouAPrimeiraMao() == 2) {
			quemJoga = 2;
		} else if (listaCartasJogadasHumano.size() == 2 && listaCartasJogadasRobo.size() == 1) {
			quemJoga = 1;
		} else if (listaCartasJogadasHumano.size() == 1 && listaCartasJogadasRobo.size() == 2) {
			quemJoga = 2;
		}

		// mão 3
		
		if (quemGanhouASegundaMao() == 1 && listaCartasJogadasHumano.size() == 2
				&& listaCartasJogadasRobo.size() == 2) {
			quemJoga = 1;

		} else if (quemGanhouASegundaMao() == 2 && listaCartasJogadasHumano.size() == 2
				&& listaCartasJogadasRobo.size() == 2) {
			quemJoga = 2;
		} else if (listaCartasJogadasHumano.size() == 3 && listaCartasJogadasRobo.size() == 2) {
			quemJoga = 1;
		} else if (contadorMao == 3 && listaCartasJogadasHumano.size() == 2 && listaCartasJogadasRobo.size() == 3) {
			quemJoga = 2;
		}

		

		return quemJoga;
	}

	public EstadoJogoModelo verificarEstadoJogo() {
		EstadoJogoModelo estadoJogo = new EstadoJogoModelo();
		estadoJogo.setQuemJogaAgora(quemJogaAgora());
		estadoJogo.setEmQueRodadaEsta(controlaPartidaNegocio.getContadorRodadas());
		estadoJogo.setPontuacaoRobo(controlaPartidaNegocio.getPontosRobo());
		estadoJogo.setPontuacaoHumano(controlaPartidaNegocio.getPontosHumano());
		estadoJogo.setQualEhAmao(contadorMao);
		estadoJogo.setQuemJogaAgora(quemJogaAgora());
		estadoJogo.setContadorJogadas(contadorJogadas);
		estadoJogo.setListaJogadasChamadas(listaJogadasChamadas);
		estadoJogo.setListaPontosChamados(listaPontosChamados);
		estadoJogo.setListaJogadasAceitas(listaJogadasAceitas);
		estadoJogo.setListaPontosAceitos(listaPontosAceitos);

		estadoJogo.setRoboTemFlor(verificarSeRoboTemFlor());

		estadoJogo.setHumanoTemFlor(verificarSeHumanoTemFlor());

		return estadoJogo;

	}

	/*
	 * 
	 * Métodos Envido
	 * 
	 */
	public boolean temPontosParaSerChamados(EstadoJogoModelo estadoJogoModelo) {
		// retorna se a lista não é vazia aí sim tem pontos para ser chamados
		return !estadoJogoModelo.listaPontosQuePodemSerChamados().isEmpty();
	}

	// verificar pontos humâno
	private boolean verificarSeJogadorRoboTemPontosParaEnvido() {
		String naipePosicaoUm = listaCartasRecebidasRobo.get(0).getNaipe();
		String naipePosicaoDois = listaCartasRecebidasRobo.get(1).getNaipe();
		String naipePosicaoTres = listaCartasRecebidasRobo.get(2).getNaipe();
		if (naipePosicaoUm.equals(naipePosicaoDois) || naipePosicaoUm.equals(naipePosicaoTres)
				|| naipePosicaoDois.equals(naipePosicaoTres))
			return true;
		return false;
	}

	public boolean verificarSeJogadorHumanoTemPontosParaEnvido() {
		String naipePosicaoUm = listaCartasRecebidasHumano.get(0).getNaipe();
		String naipePosicaoDois = listaCartasRecebidasHumano.get(1).getNaipe();
		String naipePosicaoTres = listaCartasRecebidasHumano.get(2).getNaipe();
		if (naipePosicaoUm.equals(naipePosicaoDois) || naipePosicaoUm.equals(naipePosicaoTres)
				|| naipePosicaoDois.equals(naipePosicaoTres))
			return true;
		return false;
	}

	private int calcularPontosEnvidoHumano() {
		int somaDosPontos = 0;
		if (verificarSeJogadorHumanoTemPontosParaEnvido()) {
			if (listaCartasRecebidasHumano.get(0).getNaipe().equals(listaCartasRecebidasHumano.get(1).getNaipe()))
				somaDosPontos = listaCartasRecebidasHumano.get(0).getValorQueContaParaOenvido()
						+ listaCartasRecebidasHumano.get(1).getValorQueContaParaOenvido() + 20;

			if (listaCartasRecebidasHumano.get(0).getNaipe().equals(listaCartasRecebidasHumano.get(2).getNaipe()))
				somaDosPontos = listaCartasRecebidasHumano.get(0).getValorQueContaParaOenvido()
						+ listaCartasRecebidasHumano.get(2).getValorQueContaParaOenvido() + 20;

			if (listaCartasRecebidasHumano.get(1).getNaipe().equals(listaCartasRecebidasHumano.get(2).getNaipe()))
				somaDosPontos = listaCartasRecebidasHumano.get(1).getValorQueContaParaOenvido()
						+ listaCartasRecebidasHumano.get(2).getValorQueContaParaOenvido() + 20;
		}
		// SE NÃO TIVER ENVIDO VAI PEGAR A MAIOR CARTA COM PONTOS DE ENVIDO E RETORNAR
		else {
			for (CartasModelo carta : listaCartasRecebidasHumano) {
				if (carta.getValorQueContaParaOenvido() > somaDosPontos)
					somaDosPontos = carta.getValorQueContaParaOenvido();
			}
		}

		return somaDosPontos;
	}

	private int calcularPontosEnvidoRobo() {
		int somaDosPontos = 0;
		if (verificarSeJogadorRoboTemPontosParaEnvido()) {
			if (listaCartasRecebidasRobo.get(0).getNaipe().equals(listaCartasRecebidasRobo.get(1).getNaipe()))
				somaDosPontos = listaCartasRecebidasRobo.get(0).getValorQueContaParaOenvido()
						+ listaCartasRecebidasRobo.get(1).getValorQueContaParaOenvido() + 20;

			if (listaCartasRecebidasRobo.get(0).getNaipe().equals(listaCartasRecebidasRobo.get(2).getNaipe()))
				somaDosPontos = listaCartasRecebidasRobo.get(0).getValorQueContaParaOenvido()
						+ listaCartasRecebidasRobo.get(2).getValorQueContaParaOenvido() + 20;

			if (listaCartasRecebidasRobo.get(1).getNaipe().equals(listaCartasRecebidasRobo.get(2).getNaipe()))
				somaDosPontos = listaCartasRecebidasRobo.get(1).getValorQueContaParaOenvido()
						+ listaCartasRecebidasRobo.get(2).getValorQueContaParaOenvido() + 20;
		}
		// SE NÃO TIVER ENVIDO VAI PEGAR A MAIOR CARTA COM PONTOS DE ENVIDO E RETORNAR
		else {
			for (CartasModelo carta : listaCartasRecebidasRobo) {
				if (carta.getValorQueContaParaOenvido() > somaDosPontos)
					somaDosPontos = carta.getValorQueContaParaOenvido();
			}
		}

		return somaDosPontos;
	}

	// não será tratado contra flor por que a base de casos não tem situações
	// suficientes
	public boolean verificarSeHumanoTemFlor() {
		if (listaCartasRecebidasHumano.get(0).getNaipe().equals(listaCartasRecebidasHumano.get(2).getNaipe())
				&& listaCartasRecebidasHumano.get(0).getNaipe().equals(listaCartasRecebidasHumano.get(1).getNaipe()))
			return true;
		else
			return false;

	}

	public boolean verificarSeRoboTemFlor() {

		if (listaCartasRecebidasRobo.get(0).getNaipe().equals(listaCartasRecebidasRobo.get(2).getNaipe())
				&& listaCartasRecebidasRobo.get(0).getNaipe().equals(listaCartasRecebidasRobo.get(1).getNaipe()))
			return true;
		return false;
	}

	private int calcularPontosFlorRobo() {
		int somaDosPontos = 0;
		if (verificarSeRoboTemFlor()) {
			if (listaCartasRecebidasRobo.get(0).getNaipe().equals(listaCartasRecebidasRobo.get(1).getNaipe())
					&& listaCartasRecebidasRobo.get(0).getNaipe().equals(listaCartasRecebidasRobo.get(2).getNaipe()))
				somaDosPontos = listaCartasRecebidasRobo.get(0).getValorQueContaParaOenvido()
						+ listaCartasRecebidasRobo.get(1).getValorQueContaParaOenvido()
						+ listaCartasRecebidasRobo.get(2).getValorQueContaParaOenvido() + 20;
		}

		return somaDosPontos;
	}

	private int calcularPontosFlorHumano() {
		int somaDosPontos = 0;
		if (verificarSeHumanoTemFlor()) {
			if (listaCartasRecebidasHumano.get(0).getNaipe().equals(listaCartasRecebidasHumano.get(1).getNaipe())
					&& listaCartasRecebidasHumano.get(0).getNaipe()
							.equals(listaCartasRecebidasHumano.get(2).getNaipe()))
				somaDosPontos = listaCartasRecebidasHumano.get(0).getValorQueContaParaOenvido()
						+ listaCartasRecebidasHumano.get(1).getValorQueContaParaOenvido()
						+ listaCartasRecebidasHumano.get(2).getValorQueContaParaOenvido() + 20;

		}

		return somaDosPontos;
	}

	/*
	 * o jogo sempre terá essa sequência de passos: -Aceitar pontos, aceitar
	 * chamada, se existir -Chamar Jogada(truco, retruco, quero e valeQuatro..)
	 * -jogar
	 */

	public boolean AceitarPontosRobo(String pontoSolicitadoParaSerAceito) {
		boolean aceitouChamadaDePontos = false;
		// se tiver opção para pontos não só envido, aí mudo
		boolean aceitarEnvido = trucoCbr.aceitarEnvido(atualizaConsultaCbr(), contadorMao);
		boolean aceitarRealEnvido = trucoCbr.aceitarRealEnvido(atualizaConsultaCbr(), contadorMao);
		boolean aceitarFaltaEnvido = trucoCbr.aceitarFaltaEnvido(atualizaConsultaCbr(), contadorMao);
		if (pontoSolicitadoParaSerAceito.equalsIgnoreCase("Envido") && aceitarEnvido)
			aceitouChamadaDePontos = true;
		if (pontoSolicitadoParaSerAceito.equalsIgnoreCase("RealEnvido") && aceitarRealEnvido)
			aceitouChamadaDePontos = true;
		if (pontoSolicitadoParaSerAceito.equalsIgnoreCase("FaltaEnvido") && aceitarFaltaEnvido)
			aceitouChamadaDePontos = true;

		if (aceitouChamadaDePontos) {
			JogadasAceitasModelo pontosAceitos = new JogadasAceitasModelo();
			pontosAceitos.setJogadaAceita(pontoSolicitadoParaSerAceito);
			pontosAceitos.setQuemAceitou(1);
			listaPontosAceitos.add(pontosAceitos);
			return true;

		}
		// se decidir não aceitar
		else {
			naoAceitarPontosRobo(pontoSolicitadoParaSerAceito);
			return false;
		}

	}

	private void naoAceitarPontosRobo(String jogadaNaoAceita) {
		JogadasNaoAceitasModelo jogadasNaoAceitas = new JogadasNaoAceitasModelo();
		jogadasNaoAceitas.setJogadaNaoAceita(jogadaNaoAceita);
		jogadasNaoAceitas.setQuemNaoAceitou(1);
		listPontosNaoAceitos.add(jogadasNaoAceitas);
	}

	public boolean roboVaiChamarPontos() {
		EstadoJogoModelo estadoJogo = verificarEstadoJogo();
		boolean envidoTaDisponivel = false;
		boolean RealEnvidoTaDisponivel = false;
		boolean FaltaEnvidoDisponivel = false;

		for (String ponto : estadoJogo.listaPontosQuePodemSerChamados()) {
			if (ponto.equalsIgnoreCase("Envido"))
				envidoTaDisponivel = true;
			if (ponto.equalsIgnoreCase("RealEnvido"))
				RealEnvidoTaDisponivel = true;
			if (ponto.equalsIgnoreCase("FaltaEnvido"))
				FaltaEnvidoDisponivel = true;
		}

		boolean chamarEnvido = trucoCbr.chamarEnvido(atualizaConsultaCbr(), contadorMao) && envidoTaDisponivel;
		boolean chamarRealEnvido = trucoCbr.chamarRealEnvido(atualizaConsultaCbr(), contadorMao)
				&& RealEnvidoTaDisponivel;
		boolean chamarFaltaEnvido = trucoCbr.chamarFaltaEnvido(atualizaConsultaCbr(), contadorMao)
				&& FaltaEnvidoDisponivel;

		return chamarEnvido || chamarRealEnvido || chamarFaltaEnvido;
	}

	public String chamarPontosRobo(EstadoJogoModelo estadoJogo) {
		String pontoQueSeraChamado = "";

		if (verificarSeRoboTemFlor()) {
			pontoQueSeraChamado = "flor";
		} else if (!verificarSeRoboTemFlor()) {
			boolean envidoTaDisponivel = false;
			boolean RealEnvidoTaDisponivel = false;
			boolean FaltaEnvidoDisponivel = false;

			for (String ponto : estadoJogo.listaPontosQuePodemSerChamados()) {
				if (ponto.equalsIgnoreCase("Envido"))
					envidoTaDisponivel = true;
				if (ponto.equalsIgnoreCase("RealEnvido"))
					RealEnvidoTaDisponivel = true;
				if (ponto.equalsIgnoreCase("FaltaEnvido"))
					FaltaEnvidoDisponivel = true;
			}

			boolean chamarEnvido = trucoCbr.chamarEnvido(atualizaConsultaCbr(), contadorMao);
			boolean chamarRealEnvido = trucoCbr.chamarRealEnvido(atualizaConsultaCbr(), contadorMao);
			boolean chamarFaltaEnvido = trucoCbr.chamarFaltaEnvido(atualizaConsultaCbr(), contadorMao);

			if (chamarEnvido && envidoTaDisponivel)
				pontoQueSeraChamado = "Envido";
			else if (chamarRealEnvido && RealEnvidoTaDisponivel)
				pontoQueSeraChamado = "RealEnvido";
			else if (chamarFaltaEnvido && FaltaEnvidoDisponivel)
				pontoQueSeraChamado = "FaltaEnvido";
		}

		if (!pontoQueSeraChamado.equals("")) {
			JogadasChamadasModelo pontosChamados = new JogadasChamadasModelo();
			pontosChamados.setJogadaChamada(pontoQueSeraChamado);
			pontosChamados.setQuemChamou(1);
			listaPontosChamados.add(pontosChamados);

		}

		return pontoQueSeraChamado;
	}

	public boolean AceitarJogadaRobo(String jogadaParaSerAceita) {
		EstadoJogoModelo estadoJogo = verificarEstadoJogo();
		String jogoQueSeraChamado = "";
		boolean seraAceita = false;

		boolean chamarTruco = trucoCbr.chamarTruco(atualizaConsultaCbr(), contadorMao);
		boolean chamarReTruco = trucoCbr.chamarRetruco(atualizaConsultaCbr(), contadorMao);
		boolean chamarValeQuatro = trucoCbr.chamarValeQuatro(atualizaConsultaCbr(), contadorMao);

		if (chamarTruco && jogadaParaSerAceita.equalsIgnoreCase("Truco"))
			jogoQueSeraChamado = "Truco";
		else if (chamarReTruco && jogadaParaSerAceita.equalsIgnoreCase("ReTruco"))
			jogoQueSeraChamado = "ReTruco";
		else if (chamarValeQuatro && jogadaParaSerAceita.equalsIgnoreCase("ValeQuatro"))
			jogoQueSeraChamado = "ValeQuatro";

		if (!jogoQueSeraChamado.equalsIgnoreCase(""))
			seraAceita = true;

		if (seraAceita) {
			JogadasAceitasModelo jogadaAceita = new JogadasAceitasModelo();
			jogadaAceita.setJogadaAceita(jogadaParaSerAceita);
			jogadaAceita.setQuemAceitou(1);
			listaJogadasAceitas.add(jogadaAceita);

		} else {
			JogadasNaoAceitasModelo jogadaNaoAceita = new JogadasNaoAceitasModelo();
			jogadaNaoAceita.setJogadaNaoAceita(jogadaParaSerAceita);
			jogadaNaoAceita.setQuemNaoAceitou(1);
			listaJogadasNaoAceitas.add(jogadaNaoAceita);
		}

		return seraAceita;
	}

	public boolean roboIraChamarJogada(EstadoJogoModelo estadoJogo) {
		String jogoQueSeraChamado = "";
		boolean seraChamada = false;

		boolean TrucoTaDisponivel = false;
		boolean ReTrucoTaDisponivel = false;
		boolean ValeQuatroDisponivel = false;

		for (String jogada : estadoJogo.listaJogadasQuePodemSerChamadas()) {
			if (jogada.equalsIgnoreCase("Truco"))
				TrucoTaDisponivel = true;
			if (jogada.equalsIgnoreCase("ReTruco"))
				ReTrucoTaDisponivel = true;
			if (jogada.equalsIgnoreCase("ValeQuatro"))
				ValeQuatroDisponivel = true;
		}

		boolean chamarTruco = trucoCbr.chamarTruco(atualizaConsultaCbr(), contadorMao);
		boolean chamarReTruco = trucoCbr.chamarRetruco(atualizaConsultaCbr(), contadorMao);
		boolean chamarValeQuatro = trucoCbr.chamarValeQuatro(atualizaConsultaCbr(), contadorMao);

		if (chamarTruco && TrucoTaDisponivel)
			jogoQueSeraChamado = "Truco";
		else if (chamarReTruco && ReTrucoTaDisponivel)
			jogoQueSeraChamado = "ReTruco";
		else if (chamarValeQuatro && ValeQuatroDisponivel)
			jogoQueSeraChamado = "ValeQuatro";

		if (!jogoQueSeraChamado.equalsIgnoreCase(""))
			seraChamada = true;

		return seraChamada;
	}

	public String chamarJogadaRobo(EstadoJogoModelo estadoJogo) {

		String jogoQueSeraChamado = "";
		boolean seraChamada = false;

		boolean TrucoTaDisponivel = false;
		boolean ReTrucoTaDisponivel = false;
		boolean ValeQuatroDisponivel = false;

		for (String ponto : estadoJogo.listaJogadasQuePodemSerChamadas()) {
			if (ponto.equalsIgnoreCase("Truco"))
				TrucoTaDisponivel = true;
			if (ponto.equalsIgnoreCase("ReTruco"))
				ReTrucoTaDisponivel = true;
			if (ponto.equalsIgnoreCase("ValeQuatro"))
				ValeQuatroDisponivel = true;
		}

		boolean chamarTruco = trucoCbr.chamarTruco(atualizaConsultaCbr(), contadorMao);
		boolean chamarReTruco = trucoCbr.chamarRetruco(atualizaConsultaCbr(), contadorMao);
		boolean chamarValeQuatro = trucoCbr.chamarValeQuatro(atualizaConsultaCbr(), contadorMao);

		if (chamarTruco && TrucoTaDisponivel)
			jogoQueSeraChamado = "Truco";
		else if (chamarReTruco && ReTrucoTaDisponivel)
			jogoQueSeraChamado = "ReTruco";
		else if (chamarValeQuatro && ValeQuatroDisponivel)
			jogoQueSeraChamado = "ValeQuatro";
		if (!jogoQueSeraChamado.equalsIgnoreCase("")) {
			// Adiciona as jogadas Chamadas com jogador
			JogadasChamadasModelo jogadasChamadas = new JogadasChamadasModelo();
			jogadasChamadas.setJogadaChamada(jogoQueSeraChamado);
			jogadasChamadas.setQuemChamou(1);
			jogadasChamadas.setEmQualRodada(contadorMao);
			listaJogadasChamadas.add(jogadasChamadas);
			// adiciona Jogadas que podem ser aceitas pelo jogador humâno
			JogadasQuePodemSerAceitasModelo jogadasQuePodemSerAceitasModelo = new JogadasQuePodemSerAceitasModelo();
			jogadasQuePodemSerAceitasModelo.setJogadaQuePodeSerAceita(jogoQueSeraChamado);
			jogadasQuePodemSerAceitasModelo.setQuemPodeAceitar(2);

		}
		return jogoQueSeraChamado;

	}

	public String jogarRobo(EstadoJogoModelo estadoJogo) {

		int idDaCartaQueRoboDeveJogar = 0;
		String cartaQueRoboVaiJogar;
		if (contadorMao == 1)
			idDaCartaQueRoboDeveJogar = trucoCbr.primeiraCarta(atualizaConsultaCbr(), contadorMao);
		if (contadorMao == 2)
			idDaCartaQueRoboDeveJogar = trucoCbr.segundaCarta(atualizaConsultaCbr(), contadorMao);
		if (contadorMao == 3)
			idDaCartaQueRoboDeveJogar = trucoCbr.terceiraCarta(atualizaConsultaCbr(), contadorMao);

		listaCartasJogadasRobo.add(consultaCartaPeloId(idDaCartaQueRoboDeveJogar));
		removerCartaRobo(idDaCartaQueRoboDeveJogar);

		contabilizarJogadaRobo();
		return consultaCartaPeloId(idDaCartaQueRoboDeveJogar);

	}

	public void contabilizarJogadaRobo() {
		contadorJogadas++;
		ultimoAjogar = 1;
		incrementarContadorMao();
	}

	private void incrementarContadorMao() {
		/*
		 * if (contadorJogadas == 1 || contadorJogadas == 2) contadorMao = 1; if
		 * (contadorJogadas == 3 || contadorJogadas == 4) contadorMao = 2; if
		 * (contadorJogadas == 5 || contadorJogadas == 6) contadorMao = 3;
		 */

		// será dessa forma porque o incrementar é chamado no contabilizarJogada e jogar
		// é a ultima coisa feita.
		if (contadorJogadas < 2)
			contadorMao = 1;

		if (contadorJogadas < 4 && contadorJogadas >= 2)
			contadorMao = 2;

		if (contadorJogadas >= 4 && contadorJogadas <= 6)
			contadorMao = 3;
	}

	public boolean aindaTemJogoNaRodada() {

		return (contadorJogadas < 6 ? true : false);
	}

	public String JogarHumano(String cartaQueHumanoVaiJogar) {

		listaCartasJogadasHumano.add(cartaQueHumanoVaiJogar);
		removerCartaHumano(cartaQueHumanoVaiJogar);
		contabilizarJogadaHumano();
		return cartaQueHumanoVaiJogar;

	}

	public boolean AceitarJogadaHumano(String jogadaParaSerAceita, boolean seraAceita) {
		// para teste, depois vamos ver o que chamar, verificar opções de não aceitar
		// fazer opçoes de aceitar

		if (seraAceita) {
			JogadasAceitasModelo jogadaAceita = new JogadasAceitasModelo();
			jogadaAceita.setJogadaAceita(jogadaParaSerAceita);
			jogadaAceita.setQuemAceitou(2);
			listaJogadasAceitas.add(jogadaAceita);
			seraAceita = true;
		} else {
			JogadasNaoAceitasModelo jogadaNaoAceita = new JogadasNaoAceitasModelo();
			jogadaNaoAceita.setJogadaNaoAceita(jogadaParaSerAceita);
			jogadaNaoAceita.setQuemNaoAceitou(2);
			listaJogadasNaoAceitas.add(jogadaNaoAceita);
		}

		return seraAceita;
	}

	public String chamarJogadaHumano(EstadoJogoModelo estadoJogo, String jogadaQueseraChamada) {
		List<String> listaJogadasQuePodemSerChamadas = estadoJogo.listaJogadasQuePodemSerChamadas();
		// Adiciona as jogadas Chamadas com jogador
		JogadasChamadasModelo jogadasChamadas = new JogadasChamadasModelo();
		jogadasChamadas.setJogadaChamada(jogadaQueseraChamada);
		jogadasChamadas.setQuemChamou(2);
		jogadasChamadas.setEmQualRodada(contadorMao);
		listaJogadasChamadas.add(jogadasChamadas);

		// adiciona Jogadas que podem ser aceitas pelo jogador rôbo
		JogadasQuePodemSerAceitasModelo jogadasQuePodemSerAceitasModelo = new JogadasQuePodemSerAceitasModelo();
		jogadasQuePodemSerAceitasModelo.setJogadaQuePodeSerAceita(jogadaQueseraChamada);
		jogadasQuePodemSerAceitasModelo.setQuemPodeAceitar(1);
		return jogadaQueseraChamada;

	}

	public void contabilizarJogadaHumano() {
		contadorJogadas++;
		ultimoAjogar = 2;
		incrementarContadorMao();
	}

	public void ResponderAceitarOuNaoAceitarPontosHumano(String pontoParaSerAceito, boolean respostaAceitaPontos) {
		if (respostaAceitaPontos) {
			JogadasAceitasModelo pontosAceitos = new JogadasAceitasModelo();
			pontosAceitos.setJogadaAceita(pontoParaSerAceito);
			pontosAceitos.setQuemAceitou(2);
			listaPontosAceitos.add(pontosAceitos);
		} else {
			naoAceitarPontosHumano(pontoParaSerAceito);
		}

	}

	private void naoAceitarPontosHumano(String jogadaNaoAceita) {
		JogadasNaoAceitasModelo pontosNaoAceitos = new JogadasNaoAceitasModelo();
		pontosNaoAceitos.setJogadaNaoAceita(jogadaNaoAceita);
		pontosNaoAceitos.setQuemNaoAceitou(2);
		listPontosNaoAceitos.add(pontosNaoAceitos);
	}

	public String chamarPontosHumano(boolean desejaChamarOsPontos, String pontosQueSeraoChamados) {
		String pontoQueSeraChamado = "";
		if (desejaChamarOsPontos) {
			if (verificarSeHumanoTemFlor())
				pontoQueSeraChamado = "flor";

			pontoQueSeraChamado = pontosQueSeraoChamados;

			if (!pontoQueSeraChamado.equals("")) {
				JogadasChamadasModelo pontosChamados = new JogadasChamadasModelo();
				pontosChamados.setJogadaChamada(pontoQueSeraChamado);
				pontosChamados.setQuemChamou(2);
				listaPontosChamados.add(pontosChamados);
			}

		}
		return pontoQueSeraChamado;
	}

	public QuemGanhouPontosModelo pontuaQuemGanhouPontos() {

		QuemGanhouPontosModelo quemGanhou = new QuemGanhouPontosModelo();
		if (!listaPontosAceitos.isEmpty() && listPontosNaoAceitos.isEmpty()) {
			// verifica se for envido e pontua em 2 pontos
			if (listaPontosAceitos.get(listaPontosAceitos.size() - 1).getJogadaAceita().equalsIgnoreCase("Envido")) {
				if (QuemGanhouOsPontos() == 1) {
					controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 2);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoRobo(), calcularPontosEnvidoHumano(), 1);

				}
				if (QuemGanhouOsPontos() == 2) {
					controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 2);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoHumano(), calcularPontosEnvidoRobo(), 2);
				}
			}
			// verifica se foi chamado envido e aumentado para realEnvido aí pontua em 5
			else if (listaPontosAceitos.size() >= 2
					&& listaPontosAceitos.get(listaPontosAceitos.size() - 1).getJogadaAceita()
							.equalsIgnoreCase("RealEnvido")
					&& listaPontosAceitos.get(listaPontosAceitos.size() - 2).getJogadaAceita()
							.equalsIgnoreCase("Envido")) {
				if (QuemGanhouOsPontos() == 1) {
					controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 5);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoRobo(), calcularPontosEnvidoHumano(), 1);
				}
				if (QuemGanhouOsPontos() == 2) {
					controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 5);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoHumano(), calcularPontosEnvidoRobo(), 2);
				}
			}

			// verifica se foi chamado realEnvido Sem Envido antes e pontua em 3 pontos
			else if (listaPontosAceitos.get(listaPontosAceitos.size() - 1).getJogadaAceita()
					.equalsIgnoreCase("RealEnvido")) {
				if (QuemGanhouOsPontos() == 1) {
					controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 3);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoRobo(), calcularPontosEnvidoHumano(), 1);
				}
				if (QuemGanhouOsPontos() == 2) {
					controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 3);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoHumano(), calcularPontosEnvidoRobo(), 2);
				}
			} else if (listaPontosAceitos.get(listaPontosAceitos.size() - 1).getJogadaAceita()
					.equalsIgnoreCase("FaltaEnvido")) {
				if (QuemGanhouOsPontos() == 1) {
					controlaPartidaNegocio.setPontosRobo(24);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoRobo(), calcularPontosEnvidoHumano(), 1);
				}
				if (QuemGanhouOsPontos() == 2) {
					controlaPartidaNegocio.setPontosHumano(24);
					quemGanhou = setaQuemGanhou(calcularPontosEnvidoHumano(), calcularPontosEnvidoRobo(), 2);

				}
			}
		}

		// caso o ponto chamado seja flor
		else if (!listaPontosChamados.isEmpty() && listaPontosChamados.get(listaPontosChamados.size() - 1)
				.getJogadaChamada().equalsIgnoreCase("flor")) {
			// pontua se caso só o rôbo chamou ou se o outro jogador tinha chamado também
			if ((listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 1)
					|| (listaPontosChamados.size() >= 2
							&& listaPontosChamados.get(listaPontosChamados.size() - 2).getJogadaChamada()
									.equalsIgnoreCase("flor")
							&& listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 1))
				controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 3);
			// e

			if ((listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 2)
					|| (listaPontosChamados.size() >= 2
							&& listaPontosChamados.get(listaPontosChamados.size() - 2).getJogadaChamada()
									.equalsIgnoreCase("flor")
							&& listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 2))
				controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 3);
		}

		// caso não seja aceito pontos
		if (!listPontosNaoAceitos.isEmpty()) {
			pontuaPontosNaoAceitos();

		}

		return quemGanhou;
	}

	private QuemGanhouPontosModelo setaQuemGanhou(int pontuacaoVencedor, int pontuacaoQuemPerdeu, int ganhador) {
		QuemGanhouPontosModelo quemGanhou = new QuemGanhouPontosModelo();
		quemGanhou.setPontuacaoQuePossuiGanhador(calcularPontosEnvidoRobo());
		quemGanhou.setPontuacaoQuepossuiQuemPerdeu(calcularPontosEnvidoHumano());
		quemGanhou.setQuemGanhou(1);
		return quemGanhou;
	}

	private void pontuaPontosNaoAceitos() {
		// verifica se a pontuação não aceita foi envido e pontua quem chamou em 1 ponto
		if (listPontosNaoAceitos.get(listPontosNaoAceitos.size() - 1).getJogadaNaoAceita().equals("Envido")) {
			if (listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 1 && listaPontosChamados
					.get(listaPontosChamados.size() - 1).getJogadaChamada().equalsIgnoreCase("Envido"))
				controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 1);
			if (listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 2 && listaPontosChamados
					.get(listaPontosChamados.size() - 1).getJogadaChamada().equalsIgnoreCase("Envido"))
				controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 1);
		}
		// verifica se foi chamado Envido e Real envido, pontua em 2 pontos quem chamou
		if (!listaPontosAceitos.isEmpty() && !listPontosNaoAceitos.isEmpty()
				&& listPontosNaoAceitos.get(listPontosNaoAceitos.size() - 1).getJogadaNaoAceita().equals("RealEnvido")
				&& listaPontosAceitos.get(listaPontosAceitos.size() - 1).getJogadaAceita().equals("Envido")) {
			if (listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 1 && listaPontosChamados
					.get(listaPontosChamados.size() - 1).getJogadaChamada().equalsIgnoreCase("RealEnvido"))
				controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 2);
			if (listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 2 && listaPontosChamados
					.get(listaPontosChamados.size() - 1).getJogadaChamada().equalsIgnoreCase("RealEnvido"))
				controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 2);
		} else if (listPontosNaoAceitos.get(listPontosNaoAceitos.size() - 1).getJogadaNaoAceita().equals("RealEnvido")
				&& listaPontosAceitos.isEmpty()) {
			if (listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 1 && listaPontosChamados
					.get(listaPontosChamados.size() - 1).getJogadaChamada().equalsIgnoreCase("RealEnvido"))
				controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 1);
			if (listaPontosChamados.get(listaPontosChamados.size() - 1).getQuemChamou() == 2 && listaPontosChamados
					.get(listaPontosChamados.size() - 1).getJogadaChamada().equalsIgnoreCase("RealEnvido"))
				controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 1);

		}
		// verifica quando não aceita falta envido tem que ver como funciona a pontuação
		// nesse caso.

	}

	private int QuemGanhouOsPontos() {
		if (calcularPontosEnvidoRobo() > calcularPontosEnvidoHumano())
			return 1;
		else if (calcularPontosEnvidoHumano() > calcularPontosEnvidoRobo())
			return 2;
		else if (calcularPontosEnvidoHumano() == calcularPontosEnvidoRobo())
			return quemEhMao();

		return 0;
	}

	// gets And Sets listas

	public List<JogadasAceitasModelo> getListaJogadasAceitas() {
		return listaJogadasAceitas;
	}

	public void setJogadasAceitas(List<JogadasAceitasModelo> jogadasAceitas) {
		this.listaJogadasAceitas = jogadasAceitas;
	}

	public List<JogadasAceitasModelo> getPontosAceitos() {
		return listaPontosAceitos;
	}

	public void setPontosAceitos(List<JogadasAceitasModelo> pontosAceitos) {
		this.listaPontosAceitos = pontosAceitos;
	}

	public List<String> getListaCartasJogadasRobo() {
		return listaCartasJogadasRobo;
	}

	public List<String> getListaCartasJogadasHumano() {
		return listaCartasJogadasHumano;
	}

	public List<String> getListaCartasHumanoTemNaMao() {
		List<String> listaCartasQueHumanoTemNaMaoString = new ArrayList<String>();
		for (CartasModelo cartaMao : listaCartasHumanoTemNaMao) {
			listaCartasQueHumanoTemNaMaoString.add(cartaMao.getCarta());
		}
		return listaCartasQueHumanoTemNaMaoString;
	}

	public void setListaCartasHumanoTemNaMao(List<CartasModelo> listaCartasHumanoTemNaMao) {
		this.listaCartasHumanoTemNaMao = listaCartasHumanoTemNaMao;
	}

	public void removerCartaRobo(int id) {
		CartasModelo carta = null;
		for (CartasModelo cartaModelo : listaCartasRoboTemNaMao) {
			if (cartaModelo.getId() == id) {
				carta = cartaModelo;
			}

		}
		if (carta != null)
			listaCartasRoboTemNaMao.remove(carta);
	}

	public void removerCartaHumano(String carta) {
		CartasModelo cartaRecebida = null;
		for (CartasModelo cartaModelo : listaCartasHumanoTemNaMao) {
			if (cartaModelo.getCarta().equalsIgnoreCase(carta))
				cartaRecebida = cartaModelo;

		}
		if (cartaRecebida != null)
			listaCartasHumanoTemNaMao.remove(cartaRecebida);
	}

	/* AQUI MÉTODOS PARA VERIFICAR GANHADORES JOGO E PONTUAR */
	public QuemGanhouRodadaModelo pontuaQuemGanhouArodada() {
		QuemGanhouRodadaModelo quemGanhouArodada = new QuemGanhouRodadaModelo(0, 0, 0);
		String ultimaJogadaChamada = "";
		String ultimaJogadaAceita = "";
		int quemChamouAultimaJogada = 0;
		String ultimaJogadaNaoAceita = "";

		if (!listaJogadasChamadas.isEmpty())
			ultimaJogadaChamada = listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getJogadaChamada();

		if (!listaJogadasAceitas.isEmpty())
			ultimaJogadaAceita = listaJogadasAceitas.get(listaJogadasAceitas.size() - 1).getJogadaAceita();

		if (!listaJogadasChamadas.isEmpty())
			quemChamouAultimaJogada = listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getQuemChamou();

		if (!listaJogadasNaoAceitas.isEmpty())
			ultimaJogadaNaoAceita = listaJogadasNaoAceitas.get(listaJogadasNaoAceitas.size() - 1).getJogadaNaoAceita();

		
		// caso jogada não seja aceita pelo humâno pontua rôbo
		if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita) && ultimaJogadaChamada.equalsIgnoreCase("Truco")
				&& quemChamouAultimaJogada == 1) {
			controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 1);
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 1, 2);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ReTruco") && quemChamouAultimaJogada == 1) {
			controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 2);
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 2, 2);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro") && quemChamouAultimaJogada == 1) {
			controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 3);
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 3, 2);
		}

		// caso a jogada não seja aceita pelo rôbo pontua humâno
		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("Truco") && quemChamouAultimaJogada == 2) {
			controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 1);
			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 1, 1);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ReTruco") && quemChamouAultimaJogada == 2) {
			controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 2);
			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 2, 1);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro") && quemChamouAultimaJogada == 2) {
			controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 3);
			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 3, 1);
		}

		// jogador 1 ganhou jogando quieto
		else if (quemGanhouARodada() == 1 && ultimaJogadaChamada.equalsIgnoreCase("")) {
			
			controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 1);
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 1, 0);
		}

		else if (quemGanhouARodada() == 1 && ultimaJogadaChamada.equalsIgnoreCase("Truco")
				&& ultimaJogadaAceita.equalsIgnoreCase("Truco")) {
			controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 2);
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 2, 0);
		}

		else if (quemGanhouARodada() == 1 && ultimaJogadaChamada.equalsIgnoreCase("ReTruco")
				&& ultimaJogadaAceita.equalsIgnoreCase("ReTruco")) {
			controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 3);
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 3, 0);
		}

		else if (quemGanhouARodada() == 1 && ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro")
				&& ultimaJogadaAceita.equalsIgnoreCase("ValeQuatro")) {
			controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo() + 4);
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 4, 0);
		}

		// pontua caso o humâno vença a rodada
		else if (quemGanhouARodada() == 2 && ultimaJogadaChamada.equalsIgnoreCase("")) {
			controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 1);
			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 1, 0);
		}

		else if (quemGanhouARodada() == 2 && ultimaJogadaChamada.equalsIgnoreCase("Truco")
				&& ultimaJogadaAceita.equalsIgnoreCase("Truco")) {
			controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 2);
			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 2, 0);
		}

		else if (quemGanhouARodada() == 2 && ultimaJogadaChamada.equalsIgnoreCase("ReTruco")
				&& ultimaJogadaAceita.equalsIgnoreCase("ReTruco")) {
			controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 3);
			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 3, 0);
		}

		else if (quemGanhouARodada() == 2 && ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro")
				&& ultimaJogadaAceita.equalsIgnoreCase("ValeQuatro")) {
			controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano() + 4);
			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 4, 0);
		}

		return quemGanhouArodada;
	}

	public int quemGanhouARodada() {
		// combinações em que o rôbo ganha
		if (quemGanhouAPrimeiraMao() == 1 && quemGanhouASegundaMao() == 1)
			return 1;
		if (quemGanhouAPrimeiraMao() == 1 && quemGanhouATerceiraMao() == 1)
			return 1;
		if (quemGanhouASegundaMao() == 1 && quemGanhouATerceiraMao() == 1)
			return 1;
		if (quemGanhouAPrimeiraMao() == 0 && quemGanhouASegundaMao() == 1)
			return 1;
		if (quemGanhouAPrimeiraMao() == 1 && quemGanhouASegundaMao() == 0)
			return 1;
		if (quemGanhouAPrimeiraMao() == 0 && quemGanhouASegundaMao() == 0 && quemGanhouATerceiraMao() == 1)
			return 1;

		// combinações em que o humâno ganha
		if (quemGanhouAPrimeiraMao() == 2 && quemGanhouASegundaMao() == 2)
			return 2;
		if (quemGanhouAPrimeiraMao() == 2 && quemGanhouATerceiraMao() == 2)
			return 2;
		if (quemGanhouASegundaMao() == 2 && quemGanhouATerceiraMao() == 2)
			return 2;
		if (quemGanhouAPrimeiraMao() == 0 && quemGanhouASegundaMao() == 2)
			return 2;
		if (quemGanhouAPrimeiraMao() == 2 && quemGanhouASegundaMao() == 0)
			return 2;
		if (quemGanhouAPrimeiraMao() == 0 && quemGanhouASegundaMao() == 0 && quemGanhouATerceiraMao() == 2)
			return 2;

		return 0;
	}

	// se empardar retorna 0
	private int quemGanhouAPrimeiraMao() {
		int valorImportanciaPrimeiraCartaJogadaHumano = 0;
		int valorImportanciaPrimeiraCartaRobo = 0;

		if (listaCartasJogadasHumano.size() >= 1)
			valorImportanciaPrimeiraCartaJogadaHumano = consultaValorImportanciaCarta(listaCartasJogadasHumano.get(0));
		if (listaCartasJogadasRobo.size() >= 1)
			valorImportanciaPrimeiraCartaRobo = consultaValorImportanciaCarta(listaCartasJogadasRobo.get(0));

		if (valorImportanciaPrimeiraCartaRobo > valorImportanciaPrimeiraCartaJogadaHumano)
			return 1;

		if (valorImportanciaPrimeiraCartaJogadaHumano > valorImportanciaPrimeiraCartaRobo)
			return 2;

		return 0;
	}

	// metodo teste
	private int quemGanhouMao(int mao) {
		int importanciaHumano = 0;
		int importanciaRobo = 0;

		if (listaCartasJogadasHumano.size() >= mao)
			importanciaHumano = consultaValorImportanciaCarta(listaCartasJogadasHumano.get(mao - 1));
		if (listaCartasJogadasRobo.size() >= mao)
			importanciaRobo = consultaValorImportanciaCarta(listaCartasJogadasRobo.get(mao - 1));

		if (importanciaRobo > importanciaHumano)
			return 1;

		if (importanciaHumano > importanciaRobo)
			return 2;

		return 0;
	}

	private int quemGanhouASegundaMao() {
		int valorImportanciaSegundaCartaJogadaHumano = 0;
		int valorImportanciaSegundaCartaJogadaRobo = 0;

		if (listaCartasJogadasHumano.size() >= 2)
			valorImportanciaSegundaCartaJogadaHumano = consultaValorImportanciaCarta(listaCartasJogadasHumano.get(1));
		if (listaCartasJogadasRobo.size() >= 2)
			valorImportanciaSegundaCartaJogadaRobo = consultaValorImportanciaCarta(listaCartasJogadasRobo.get(1));

		if (valorImportanciaSegundaCartaJogadaRobo > valorImportanciaSegundaCartaJogadaHumano)
			return 1;

		if (valorImportanciaSegundaCartaJogadaHumano > valorImportanciaSegundaCartaJogadaRobo)
			return 2;

		return 0;
	}

	private int quemGanhouATerceiraMao() {
		int valorImportanciaTerceiraCartaJogadaHumano = 0;
		int valorImportanciaTerceiraCartaJogadaRobo = 0;

		if (listaCartasJogadasHumano.size() >= 3)
			valorImportanciaTerceiraCartaJogadaHumano = consultaValorImportanciaCarta(listaCartasJogadasHumano.get(2));
		if (listaCartasJogadasRobo.size() >= 3)
			valorImportanciaTerceiraCartaJogadaRobo = consultaValorImportanciaCarta(listaCartasJogadasRobo.get(2));

		if (valorImportanciaTerceiraCartaJogadaRobo > valorImportanciaTerceiraCartaJogadaHumano)
			return 1;

		if (valorImportanciaTerceiraCartaJogadaHumano > valorImportanciaTerceiraCartaJogadaRobo)
			return 2;

		return 0;
	}

	private int consultaValorImportanciaCarta(String cartaSolicitada) {
		int valorImportanciaCarta = 0;
		for (CartasModelo cartas : listaCartas) {
			if (cartas.getCarta().equalsIgnoreCase(cartaSolicitada))
				valorImportanciaCarta = cartas.getValorImportancia();

		}
		return valorImportanciaCarta;
	}

	private String consultaCartaPeloId(int idCartaSolicitada) {
		String carta = "";
		for (CartasModelo cartas : listaCartas) {
			if (cartas.getId() == idCartaSolicitada)
				carta = cartas.getCarta();

		}
		return carta;
	}

	private int consultaIdCarta(String carta) {
		int id = 0;
		for (CartasModelo cartas : listaCartas) {
			if (cartas.getCarta() == carta)
				id = cartas.getId();

		}
		return id;
	}

	private List<CartaPorImportanciaModelo> retornaListaDeCartasPorImportancia(
			List<CartasModelo> listaCartasPorJogador) {
		return new AuxiliarConsultaRoboNegocio().retornaListaCartaPorImportancia(listaCartasPorJogador);
	}

	// verificar se humâno mentiu no envido
	public int humanoMentiuNoEnvido() {
		int mentiu = 0;
		if (calcularPontosEnvidoHumano() < 20 && !listaPontosChamados.isEmpty()) {
			for (JogadasChamadasModelo pontosChamados : listaPontosChamados) {
				if (pontosChamados.getJogadaChamada().equalsIgnoreCase("Envido"))
					return 1;
				else if (pontosChamados.getJogadaChamada().equalsIgnoreCase("RealEnvido"))
					return 1;

				else if (pontosChamados.getJogadaChamada().equalsIgnoreCase("FaltaEnvido"))
					return 1;

			}
		}
		return mentiu;
	}

	public int roboMentiuNoEnvido() {
		int mentiu = -1;
		if (calcularPontosEnvidoRobo() < 20 && !listaPontosAceitos.isEmpty() && !verificarSeRoboTemFlor()
				&& !verificarSeHumanoTemFlor()) {
			for (JogadasChamadasModelo pontosChamados : listaPontosChamados) {
				if (pontosChamados.getJogadaChamada().equalsIgnoreCase("Envido"))
					return 1;
				else if (pontosChamados.getJogadaChamada().equalsIgnoreCase("RealEnvido"))
					return 1;

				else if (pontosChamados.getJogadaChamada().equalsIgnoreCase("FaltaEnvido"))
					return 1;

			}
		}
		return mentiu;
	}

	private int humanoMentiuNaFlor() {
		int mentiu = 0;
		if (!verificarSeHumanoTemFlor() && !listaPontosChamados.isEmpty()) {
			for (JogadasChamadasModelo pontosChamados : listaPontosChamados) {
				if (pontosChamados.getJogadaChamada().equalsIgnoreCase("Flor"))
					return 1;

			}
		}
		return mentiu;
	}

	private int roboMentiuNaFlor() {
		int mentiu = 0;
		if (!verificarSeRoboTemFlor() && !listaPontosChamados.isEmpty()) {
			for (JogadasChamadasModelo pontosChamados : listaPontosChamados) {
				if (pontosChamados.getJogadaChamada().equalsIgnoreCase("Flor"))
					return 1;

			}
		}
		return mentiu;
	}

	public JogadasChamadasModelo retornaPontosChamado(String qualPonto) {
		JogadasChamadasModelo jogadaChamada = null;
		for (JogadasChamadasModelo pontoChamado : listaPontosChamados) {
			if (pontoChamado.getJogadaChamada().equalsIgnoreCase(qualPonto)) {
				jogadaChamada = new JogadasChamadasModelo();
				jogadaChamada.setJogadaChamada(pontoChamado.getJogadaChamada());
				jogadaChamada.setQuemChamou(pontoChamado.getQuemChamou());
			}
		}

		return jogadaChamada;
	}

	public int retornaTentosEnvidoDeQuemGanhouPontos() {
		int maiorPontuacao = -1;
		for (JogadasAceitasModelo pontoAceito : listaJogadasAceitas) {
			if (pontoAceito.getJogadaAceita().equalsIgnoreCase("Envido")
					|| pontoAceito.getJogadaAceita().equalsIgnoreCase("RealEnvido")
					|| pontoAceito.getJogadaAceita().equalsIgnoreCase("FaltaEnvido")) {
				if (calcularPontosEnvidoHumano() > calcularPontosEnvidoRobo()) {
					maiorPontuacao = calcularPontosEnvidoHumano();
				} else
					maiorPontuacao = calcularPontosEnvidoRobo();

			}

		}
		return maiorPontuacao;

	}

	public int retornaQuemGanhouAflor() {
		int quemGanhou = 0;
		if (verificarSeHumanoTemFlor() && !verificarSeRoboTemFlor())
			quemGanhou = 2;
		else if (!verificarSeHumanoTemFlor() && verificarSeRoboTemFlor())
			quemGanhou = 1;
		else if (verificarSeHumanoTemFlor() && verificarSeRoboTemFlor()
				&& calcularPontosFlorRobo() > calcularPontosFlorHumano())
			quemGanhou = 1;
		else if (verificarSeHumanoTemFlor() && verificarSeRoboTemFlor()
				&& calcularPontosFlorRobo() > calcularPontosFlorHumano())
			quemGanhou = 2;

		return quemGanhou;

	}

	public int retornaPontosNegados() {
		int jogadaNaoAceita = -1;
		for (JogadasNaoAceitasModelo pontoNaoAceito : listPontosNaoAceitos) {

			if (pontoNaoAceito.getJogadaNaoAceita().equalsIgnoreCase("Envido")
					&& pontoNaoAceito.getQuemNaoAceitou() == 1)
				jogadaNaoAceita = 0;

			if (pontoNaoAceito.getJogadaNaoAceita().equalsIgnoreCase("Envido")
					&& pontoNaoAceito.getQuemNaoAceitou() == 2)
				jogadaNaoAceita = 1;

			if (pontoNaoAceito.getJogadaNaoAceita().equalsIgnoreCase("FaltaEnvido")
					&& pontoNaoAceito.getQuemNaoAceitou() == 1)
				jogadaNaoAceita = 2;

			if (pontoNaoAceito.getJogadaNaoAceita().equalsIgnoreCase("FaltaEnvido")
					&& pontoNaoAceito.getQuemNaoAceitou() == 2)
				jogadaNaoAceita = 3;

			if (pontoNaoAceito.getJogadaNaoAceita().equalsIgnoreCase("RealEnvido")
					&& pontoNaoAceito.getQuemNaoAceitou() == 1)
				jogadaNaoAceita = 4;

			if (pontoNaoAceito.getJogadaNaoAceita().equalsIgnoreCase("RealEnvido")
					&& pontoNaoAceito.getQuemNaoAceitou() == 2)
				jogadaNaoAceita = 5;

		}

		return jogadaNaoAceita;
	}

	public void irAoBaralho(int jogadorQueFoiAoBaralho) {
		if(jogadorQueFoiAoBaralho == 1) controlaPartidaNegocio.setPontosHumano(controlaPartidaNegocio.getPontosHumano()+1);
		if(jogadorQueFoiAoBaralho == 2) controlaPartidaNegocio.setPontosRobo(controlaPartidaNegocio.getPontosRobo()+1);
	}
	
	public void armazenaTentosInicioDaRodada() {
		controlaPartidaNegocio.setPontosAnterioresHumano(controlaPartidaNegocio.getPontosHumano());
		controlaPartidaNegocio.setPontosAnterioresRobo(controlaPartidaNegocio.getPontosRobo());
	}

	public TrucoDescription atualizaConsultaCbr() {
		TrucoDescription cbr = new TrucoDescription();
		// cbr.setHumanoCartaVirada(humanoCartaVirada);

		// cbr.setCASEID(String cASEID);

		cbr.setJogadorMao(quemEhMao());

		cbr.setCartaAltaRobo(retornaListaDeCartasPorImportancia(listaCartasRecebidasRobo).get(0).getId());

		cbr.setCartaMediaRobo(retornaListaDeCartasPorImportancia(listaCartasRecebidasRobo).get(1).getId());

		cbr.setCartaBaixaRobo(retornaListaDeCartasPorImportancia(listaCartasRecebidasRobo).get(2).getId());

		if (listaCartasJogadasHumano.size() == 3)
			cbr.setCartaAltaHumano(retornaListaDeCartasPorImportancia(listaCartasRecebidasHumano).get(0).getId());

		if (listaCartasJogadasHumano.size() == 3)
			cbr.setCartaMediaHumano(retornaListaDeCartasPorImportancia(listaCartasRecebidasHumano).get(1).getId());

		if (listaCartasJogadasHumano.size() == 3)
			cbr.setCartaBaixaHumano(retornaListaDeCartasPorImportancia(listaCartasRecebidasHumano).get(2).getId());

		if (contadorMao >= 1 && listaCartasJogadasRobo.size() >= 1)
			cbr.setPrimeiraCartaRobo(consultaIdCarta(listaCartasJogadasRobo.get(0)));

		// cbr.setPrimeiraCartaHumano(Integer primeiraCartaHumano);
		if (contadorMao >= 1 && listaCartasJogadasHumano.size() >= 1)
			cbr.setPrimeiraCartaHumano(consultaIdCarta(listaCartasJogadasHumano.get(0)));

		// cbr.setSegundaCartaRobo(Integer segundaCartaRobo);
		if (contadorMao >= 2 && listaCartasJogadasRobo.size() >= 2)
			cbr.setSegundaCartaRobo(consultaIdCarta(listaCartasJogadasRobo.get(1)));

		if (contadorMao >= 2 && listaCartasJogadasHumano.size() >= 2)
			cbr.setSegundaCartaHumano(consultaIdCarta(listaCartasJogadasHumano.get(1)));

		if (contadorMao >= 3 && listaCartasJogadasRobo.size() >= 3)
			cbr.setTerceiraCartaRobo(consultaIdCarta(listaCartasJogadasRobo.get(2)));

		if (contadorMao >= 3 && listaCartasJogadasHumano.size() >= 3)
			cbr.setTerceiraCartaHumano(consultaIdCarta(listaCartasJogadasHumano.get(2)));

		if (contadorMao > 1)
			cbr.setGanhadorPrimeiraRodada(quemGanhouAPrimeiraMao());

		if (contadorMao > 2)
			cbr.setGanhadorSegundaRodada(quemGanhouASegundaMao());

		if (contadorMao > 3)
			cbr.setGanhadorTerceiraRodada(quemGanhouATerceiraMao());
		// cbr.setRoboCartaVirada(Integer roboCartaVirada);
		// cbr.setHumanoCartaVirada(Integer humanoCartaVirada);
		if (retornaPontosChamado("Envido") != null)
			cbr.setQuemPediuEnvido(retornaPontosChamado("Envido").getQuemChamou());

		if (retornaPontosChamado("FaltaEnvido") != null)
			cbr.setQuemPediuFaltaEnvido(retornaPontosChamado("FaltaEnvido").getQuemChamou());

		if (retornaPontosChamado("RealEnvido") != null)
			cbr.setQuemPediuRealEnvido(retornaPontosChamado("RealEnvido").getQuemChamou());

		cbr.setPontosEnvidoRobo(calcularPontosEnvidoRobo());
		cbr.setPontosEnvidoHumano(calcularPontosEnvidoHumano());

		if (retornaPontosNegados() != -1)
			cbr.setQuemNegouEnvido(retornaPontosNegados());

		if (contadorMao > 1 && (retornaPontosChamado("Envido") != null || retornaPontosChamado("RealEnvido") != null
				|| retornaPontosChamado("FaltaEnvido") != null))
			cbr.setQuemGanhouEnvido(QuemGanhouOsPontos());

		if (contadorMao > 1 && retornaTentosEnvidoDeQuemGanhouPontos() != -1)
			cbr.setTentosEnvido(retornaTentosEnvidoDeQuemGanhouPontos());

		// setado 2 se humano tiver flor, 1 rôbo e 0 se nenhum
		if (verificarSeHumanoTemFlor())
			cbr.setQuemFlor(2);
		else if (verificarSeRoboTemFlor())
			cbr.setQuemFlor(1);
		else if (!verificarSeHumanoTemFlor() && !verificarSeRoboTemFlor())
			cbr.setQuemFlor(0);

		// cbr.setQuemContraFlor(Integer quemContraFlor);
		// cbr.setQuemContraFlorResto(Integer quemContraFlorResto);
		// cbr.setQuemNegouFlor(Integer quemNegouFlor);
		if (verificarSeRoboTemFlor())
			cbr.setPontosFlorRobo(calcularPontosFlorRobo());

		if (verificarSeHumanoTemFlor())
			cbr.setPontosFlorHumano(calcularPontosFlorHumano());

		cbr.setQuemGanhouFlor(retornaQuemGanhouAflor());

		if (retornaQuemGanhouAflor() == 1)
			cbr.setTentosFlor(calcularPontosFlorRobo());
		else if (retornaQuemGanhouAflor() == 2)
			cbr.setTentosFlor(calcularPontosFlorHumano());
		else
			cbr.setTentosFlor(0);
		// cbr.setQuemEscondeuPontosEnvido(Integer quemEscondeuPontosEnvido);
		// cbr.setQuemEscondeuPontosFlor(Integer quemEscondeuPontosFlor);

		if (!listaJogadasChamadas.isEmpty() && verificaInformacoesJogada("Truco") != null)
			cbr.setQuemTruco(verificaInformacoesJogada("Truco").getQuemChamou());

		if (!listaJogadasChamadas.isEmpty() && verificaInformacoesJogada("Truco") != null)
			cbr.setQuandoTruco(verificaInformacoesJogada("Truco").getEmQualRodada());

		if (!listaJogadasChamadas.isEmpty() && verificaInformacoesJogada("ReTruco") != null)
			cbr.setQuemRetruco(verificaInformacoesJogada("ReTruco").getQuemChamou());

		if (!listaJogadasChamadas.isEmpty() && verificaInformacoesJogada("ReTruco") != null)
			cbr.setQuandoRetruco(verificaInformacoesJogada("ReTruco").getEmQualRodada());

		if (!listaJogadasChamadas.isEmpty() && verificaInformacoesJogada("ValeQuatro") != null)
			cbr.setQuemValeQuatro(verificaInformacoesJogada("ValeQuatro").getQuemChamou());

		if (!listaJogadasChamadas.isEmpty() && verificaInformacoesJogada("ValeQuatro") != null)
			cbr.setQuandoValeQuatro(verificaInformacoesJogada("ValeQuatro").getEmQualRodada());

		if (!listaJogadasNaoAceitas.isEmpty())
			cbr.setQuemNegouTruco(quemNegouTruco());

		if (!listaJogadasAceitas.isEmpty())
			cbr.setQuemGanhouTruco(verificaQuemGanhouOjogo().getQuemGanhou());

		if (!listaJogadasAceitas.isEmpty())
			cbr.setTentosTruco(verificaQuemGanhouOjogo().getQuantosPontosGanhou());

		cbr.setTentosAnterioresRobo(controlaPartidaNegocio.getPontosAnterioresRobo());

		cbr.setTentosAnterioresHumano(controlaPartidaNegocio.getPontosAnterioresHumano());

		if (contadorJogadas > 2)
			cbr.setTentosPosterioresRobo(controlaPartidaNegocio.getPontosRobo());

		cbr.setTentosPosterioresHumano(controlaPartidaNegocio.getPontosHumano());

		if (contadorJogadas > 1)
			cbr.setRoboMentiuEnvido(roboMentiuNoEnvido());

		if (contadorMao > 1)
			cbr.setHumanoMentiuEnvido(humanoMentiuNoEnvido());

		cbr.setRoboMentiuFlor(roboMentiuNaFlor());

		if (contadorMao > 1)
			cbr.setHumanoMentiuFlor(humanoMentiuNaFlor());

		// cbr.setRoboMentiuTruco(Integer roboMentiuTruco);
		// cbr.setHumanoMentiuTruco(Integer humanoMentiuTruco);
		// cbr.setQuemBaralho(Integer quemBaralho);

		return cbr;

	}
	//ver aqui
	public boolean jogadorHumanoMentiuNoTruco() {
	return	(retornaListaDeCartasPorImportancia(listaCartasRecebidasHumano).get(0).getValorImportancia()<7 );
		
									}

	public boolean jogadorRoboMentiuNoTruco() {
		return	(retornaListaDeCartasPorImportancia(listaCartasRecebidasRobo).get(0).getValorImportancia()<7 );
			
										}
	
	
	public int quemNegouTruco() {
		int retorno = -1;
		String ultimaJogadaChamada = "";
		String ultimaJogadaAceita = "";
		int quemChamouAultimaJogada = 0;
		String ultimaJogadaNaoAceita = "";

		if (!listaJogadasChamadas.isEmpty())
			ultimaJogadaChamada = listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getJogadaChamada();

		if (!listaJogadasAceitas.isEmpty())
			ultimaJogadaAceita = listaJogadasAceitas.get(listaJogadasAceitas.size() - 1).getJogadaAceita();

		if (!listaJogadasChamadas.isEmpty())
			quemChamouAultimaJogada = listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getQuemChamou();

		if (!listaJogadasNaoAceitas.isEmpty())
			ultimaJogadaNaoAceita = listaJogadasNaoAceitas.get(listaJogadasNaoAceitas.size() - 1).getJogadaNaoAceita();

		if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita) && ultimaJogadaChamada.equalsIgnoreCase("Truco")
				&& quemChamouAultimaJogada == 1) {
			retorno = 1;

		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ReTruco") && quemChamouAultimaJogada == 1) {

			retorno = 3;
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro") && quemChamouAultimaJogada == 1) {
			retorno = 5;
		}

		// caso a jogada não seja aceita pelo rôbo pontua humâno
		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("Truco") && quemChamouAultimaJogada == 2) {
			retorno = 0;
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ReTruco") && quemChamouAultimaJogada == 2) {

			retorno = 2;
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro") && quemChamouAultimaJogada == 2) {
			retorno = 4;
		}

		return retorno;

	}

	public JogadasChamadasModelo verificaInformacoesJogada(String jogada) {
		JogadasChamadasModelo retorno = null;

		for (JogadasChamadasModelo jogadaChamada : listaJogadasChamadas) {
			if (jogadaChamada.getJogadaChamada().equalsIgnoreCase(jogada))
				retorno = jogadaChamada;
		}

		return retorno;
	}

	public QuemGanhouRodadaModelo verificaQuemGanhouOjogo() {
		QuemGanhouRodadaModelo quemGanhouArodada = new QuemGanhouRodadaModelo(0, 0, 0);
		String ultimaJogadaChamada = "";
		String ultimaJogadaAceita = "";
		int quemChamouAultimaJogada = 0;
		String ultimaJogadaNaoAceita = "";

		if (!listaJogadasChamadas.isEmpty())
			ultimaJogadaChamada = listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getJogadaChamada();

		if (!listaJogadasAceitas.isEmpty())
			ultimaJogadaAceita = listaJogadasAceitas.get(listaJogadasAceitas.size() - 1).getJogadaAceita();

		if (!listaJogadasChamadas.isEmpty())
			quemChamouAultimaJogada = listaJogadasChamadas.get(listaJogadasChamadas.size() - 1).getQuemChamou();

		if (!listaJogadasNaoAceitas.isEmpty())
			ultimaJogadaNaoAceita = listaJogadasNaoAceitas.get(listaJogadasNaoAceitas.size() - 1).getJogadaNaoAceita();

		// caso jogada não seja aceita pelo humâno pontua rôbo
		if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita) && ultimaJogadaChamada.equalsIgnoreCase("Truco")
				&& quemChamouAultimaJogada == 1) {
			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 1, 2);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ReTruco") && quemChamouAultimaJogada == 1) {

			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 2, 2);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro") && quemChamouAultimaJogada == 1) {

			quemGanhouArodada = new QuemGanhouRodadaModelo(1, 3, 2);
		}

		// caso a jogada não seja aceita pelo rôbo pontua humâno
		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("Truco") && quemChamouAultimaJogada == 2) {

			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 1, 1);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ReTruco") && quemChamouAultimaJogada == 2) {

			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 2, 1);
		}

		else if (ultimaJogadaChamada.equalsIgnoreCase(ultimaJogadaNaoAceita)
				&& ultimaJogadaChamada.equalsIgnoreCase("ValeQuatro") && quemChamouAultimaJogada == 2) {

			quemGanhouArodada = new QuemGanhouRodadaModelo(2, 3, 1);
		}
		return quemGanhouArodada;
	}

	
	
}
