package trabalho.jogo.ui.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;

public class MessageAceitar {


 public boolean criaAlerta(String contentText, List<String> oquePodeSerAceito, String titleText){
	 boolean resposta= false;
	 List<String> choices = new ArrayList<String>();
	 choices.add("QUERO");
	 choices.add("NÃO QUERO");

	 ChoiceDialog<String> dialog = new ChoiceDialog<>("QUERO", choices);
	 dialog.setTitle(titleText);
	 //pega a ultima posição que é a ultima jogada chamada tem que ser -2 que a ultima sempre será não aceitar
	 dialog.setHeaderText(oquePodeSerAceito.get(oquePodeSerAceito.size()-2));
	 dialog.setContentText(contentText);

	 // Traditional way to get the response value.
	 Optional<String> result = dialog.showAndWait();
	 if (result.isPresent()){
	    if(result.get().equalsIgnoreCase("QUERO")) resposta = true;
	 }

	 return resposta;   
	}
}

