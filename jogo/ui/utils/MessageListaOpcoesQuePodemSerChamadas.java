package trabalho.jogo.ui.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;

public class MessageListaOpcoesQuePodemSerChamadas {


 public String criaAlerta(String contentText, List<String> oquePodeSerChamado, String titleText){
	 String resposta = "";
	 List<String> choices = new ArrayList<String>();
	 for(String opcao : oquePodeSerChamado) {
		 choices.add(opcao);
	 }

	 ChoiceDialog<String> dialog = new ChoiceDialog<>(oquePodeSerChamado.get(oquePodeSerChamado.size()-1), choices);
	 dialog.setTitle(titleText);
	 dialog.setHeaderText("O que deseja chamar?");
	 dialog.setContentText(contentText);

	 // Traditional way to get the response value.
	 Optional<String> result = dialog.showAndWait();
	 if (result.isPresent()){
	    resposta= result.get();
	 }

	 return resposta;   
	}
}

