package trabalho.jogo.ui.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceDialog;

public class InformationMensagem {
	 public void criaAlerta(String contentText,String headerText , String titleText){
		 Alert alerta = new Alert(Alert.AlertType.INFORMATION);
	     alerta.setContentText(contentText);
	     alerta.setHeaderText(headerText);
	     alerta.setTitle(titleText);
	     alerta.show();

	 }
}
