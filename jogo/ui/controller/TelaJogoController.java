package trabalho.jogo.ui.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import com.sun.javafx.scene.control.ControlAcceleratorSupport;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import trabalho.jogo.jogoNegocio.ControlaPartidaNegocio;
import trabalho.jogo.jogoNegocio.ControlaRodadaNegocio;
import trabalho.jogo.models.EstadoJogoModelo;
import trabalho.jogo.models.QuemGanhouPontosModelo;
import trabalho.jogo.models.QuemGanhouRodadaModelo;
import trabalho.jogo.ui.utils.InformationMensagem;
import trabalho.jogo.ui.utils.MensagemAceitarChamadas;
import trabalho.jogo.ui.utils.MessageAceitar;
import trabalho.jogo.ui.utils.MessageListaOpcoesQuePodemSerChamadas;

public class TelaJogoController implements Initializable {
	// Instancia
	ControlaRodadaNegocio controlaRodadaNegocio;
	EstadoJogoModelo estadoJogoModelo;

	// componentes
	@FXML
	private Label lblCartaUm;

	@FXML
	private Label lblCartaDois;

	@FXML
	private Label lblCartaTres;

	@FXML
	private ComboBox<String> cmbSelecionarCartaParaJogar;

	@FXML
	private Button btnJogar;

	@FXML
	private ComboBox<String> cmbOqueChamar;

	@FXML
	private Button btnChamar;

	@FXML
	private Label lblCartaRobo;

	@FXML
	private Label lblRespostaRobo;

	@FXML
	private Label lblAvisosJogo;

	@FXML
	private Button btnIniciarRodada;

	@FXML
	private ListView<String> listViewHumano;

	@FXML
	private ListView<String> listViewRobo;

	@FXML
	private Label lblPontosHumano;

	@FXML
	private Label lblPontosRobo;

	@FXML
	private Label lblChamadasRobo;

	public void initialize(URL url, ResourceBundle rb) {
		btnJogar.setDisable(true);
		btnChamar.setDisable(true);

	}

	@FXML
	public void IniciarRodada() {
		iniciaTela();
		controlaRodadaNegocio = new ControlaRodadaNegocio();
		controlaRodadaNegocio.darCartasRobo();
		
		List<String> listaCartasHumano = new ArrayList<String>();
		listaCartasHumano = controlaRodadaNegocio.darCartasHumano();
		btnIniciarRodada.setDisable(true);

		// adiciona valor das cartas nas labels
		lblCartaUm.setText(listaCartasHumano.get(0));
		lblCartaDois.setText(listaCartasHumano.get(1));
		lblCartaTres.setText(listaCartasHumano.get(2));
		// recebe o objeto com o estado do jogo
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();

		// se é o humâno quem joga, habilita opção para humâno jogar
		if (estadoJogoModelo.getQuemJogaAgora() == 2) {
			jogarHumano();

		}

		// se for o rôbo
		if (estadoJogoModelo.getQuemJogaAgora() == 1) {

			jogarRobo();
		}

	}

	private List<String> atualizaCombosHumano() {
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
		// cartas para Jogar
		List<String> listaCartasHumano = new ArrayList<String>();

		listaCartasHumano = controlaRodadaNegocio.getListaCartasHumanoTemNaMao();

		ObservableList<String> observable = FXCollections.observableArrayList(listaCartasHumano);
		cmbSelecionarCartaParaJogar.setItems(observable);

		// jogadas que podem ser chamadas
		List<String> listaJogadasQuePodemSerChamadasHumano = new ArrayList<String>();
		listaJogadasQuePodemSerChamadasHumano = estadoJogoModelo.listaJogadasQuePodemSerChamadas();
		ObservableList<String> observableJogadas = FXCollections
				.observableArrayList(listaJogadasQuePodemSerChamadasHumano);
		cmbOqueChamar.setItems(observableJogadas);

		return listaCartasHumano;
	}

	private void jogarRobo() {

		//controlaRodadaNegocio.contabilizarJogadaRobo();
		lblAvisosJogo.setText("Vez do robô jogar!");
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
		// verificar se vai Chamar Ponto Ou não, caso sim, habilita para humâno aceitar
		// pontuação
		
		if (controlaRodadaNegocio.roboVaiChamarPontos() && estadoJogoModelo.getQualEhAmao() == 1
				&& controlaRodadaNegocio.getPodeChamarPontosAinda()) {
			estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
			perguntaAceitarPontosHumano();

		}

		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
		// aqui verifica se robo vai chamar jogo ou não

		if ((controlaRodadaNegocio.roboIraChamarJogada(estadoJogoModelo))
				&& (controlaRodadaNegocio.quemPodeChamarJogoAgora() == 1
						|| controlaRodadaNegocio.quemPodeChamarJogoAgora() == 0)) {
			String jogoQueSeraChamado = controlaRodadaNegocio.chamarJogadaRobo(estadoJogoModelo);
			lblChamadasRobo.setText("Quero " + jogoQueSeraChamado);
			roboPerguntaSeHumanoAceitaJogo(jogoQueSeraChamado);
		}
		largarCartaRobo();
	}

	public void roboPerguntaSeHumanoAceitaJogo(String jogadaQueSeraChamada) {
		boolean respostaDesejaAceitarChamadaJogo = new MensagemAceitarChamadas().criaAlerta("Deseja aceitar?",
				jogadaQueSeraChamada, "Rôbo chamou o jogo");
		controlaRodadaNegocio.AceitarJogadaHumano(jogadaQueSeraChamada, respostaDesejaAceitarChamadaJogo);
		if (!respostaDesejaAceitarChamadaJogo) {
			QuemGanhouRodadaModelo ganhou = controlaRodadaNegocio.pontuaQuemGanhouArodada();

			/*
			 * new InformationMensagem().criaAlerta("Vôcê não aceitou", "Rôbo ganhou" +
			 * ganhou.getQuantosPontosGanhou() + " pontos", "Fim da rodada");
			 * 
			 */
			atualizarMarcadorPontos();
			IniciarRodada();
		}

	}

	@FXML
	private void largarCartaHumano() {
		String cartaSelecionadaParaJogarHumano = cmbSelecionarCartaParaJogar.getValue();
		controlaRodadaNegocio.JogarHumano(cartaSelecionadaParaJogarHumano);

		ObservableList<String> observableListCartasJogadasHumano = FXCollections
				.observableArrayList(controlaRodadaNegocio.getListaCartasJogadasHumano());
		listViewHumano.setItems(observableListCartasJogadasHumano);

		if (lblCartaUm.getText().equalsIgnoreCase(cartaSelecionadaParaJogarHumano))
			lblCartaUm.setText("");
		if (lblCartaDois.getText().equalsIgnoreCase(cartaSelecionadaParaJogarHumano))
			lblCartaDois.setText("");
		if (lblCartaTres.getText().equalsIgnoreCase(cartaSelecionadaParaJogarHumano))
			lblCartaTres.setText("");

		atualizaCombosHumano();

		verificaSeRoboQuejogaOuFimDeJogo();
		verificaSeHumanoQuejogaOuFimDeJogo();
	}

	private void largarCartaRobo() {
		lblCartaRobo.setText(controlaRodadaNegocio.jogarRobo(estadoJogoModelo));
		// ListView CartasJogadasRobo
		ObservableList<String> observableListCartasJogadasRobo = FXCollections
				.observableArrayList(controlaRodadaNegocio.getListaCartasJogadasRobo());
		listViewRobo.setItems(observableListCartasJogadasRobo);

		verificaSeHumanoQuejogaOuFimDeJogo();
		verificaSeRoboQuejogaOuFimDeJogo();
	}

	private void jogarHumano() {
		//controlaRodadaNegocio.contabilizarJogadaHumano();
		boolean jaPerguntouSeHumanoQuerChamarOsPontos= false;
		lblAvisosJogo.setText("Sua vez de Jogar!");
		
		if (controlaRodadaNegocio.getPodeChamarPontosAinda() && estadoJogoModelo.getQualEhAmao() == 1
				&& jaPerguntouSeHumanoQuerChamarOsPontos == false)
		
			jaPerguntouSeHumanoQuerChamarOsPontos = perguntaAceitarPontosRobo();

		// verificar chamar pontos humâno
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();

		// habilita interface
		atualizaCombosHumano();

		// habilita botões
		btnJogar.setDisable(false);
		btnChamar.setDisable(false);

	}

	@FXML
	public void humanoPerguntaSeRoboAceitaJogo() {
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
		controlaRodadaNegocio.chamarJogadaHumano(estadoJogoModelo, cmbOqueChamar.getValue());
		boolean roboAceitou = controlaRodadaNegocio.AceitarJogadaRobo(cmbOqueChamar.getValue());
		if (!roboAceitou) {
			QuemGanhouRodadaModelo ganhou = controlaRodadaNegocio.pontuaQuemGanhouArodada();
			/*
			 * new InformationMensagem() .criaAlerta("Rôbo não aceitou", "você ganhou " +
			 * ganhou.getQuantosPontosGanhou() + " quemGanhou: " + ganhou.getQuemGanhou() +
			 * " quem não aceitou" + ganhou.getQuemNaoAceitouPontos() + " pontos",
			 * "Fim da rodada");
			 */

			lblAvisosJogo.setText("você ganhou" + ganhou.getQuantosPontosGanhou());
			atualizarMarcadorPontos();
			IniciarRodada();
		}

	}

	// começa o humâno perguntando pontos para o rôbo
	public boolean perguntaAceitarPontosRobo() {
		// verifica se jogador humâno quer chamar pontos
		boolean respostaDesejaChamarPontos = new MessageAceitar().criaAlerta("deseja chamar os pontos",
				estadoJogoModelo.listaPontosQuePodemSerChamados(), "Você que joga, quer chamar os pontos?");

		if (respostaDesejaChamarPontos) {
			// atualiza o estado do jogo
			estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();

			controlaRodadaNegocio.setPontosJaChamados(true);
			// se quiser chamar habilita opções
			String pontoChamado = new MessageListaOpcoesQuePodemSerChamadas().criaAlerta("Qual ponto irá chamar?",
					estadoJogoModelo.listaPontosQuePodemSerChamados(), "Ponto Que Irá Chamar");
			controlaRodadaNegocio.chamarPontosHumano(true, pontoChamado);

			// verificar se rôbo quer aceitar
			if (controlaRodadaNegocio.AceitarPontosRobo(pontoChamado)) {
				// verificar se rôbo deseja aumentar
				String chamadaDePontosRobo = controlaRodadaNegocio.chamarPontosRobo(estadoJogoModelo);
				if (!chamadaDePontosRobo.equals("")) {
					estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();

					boolean respostaAceitaPontos = new MensagemAceitarChamadas().criaAlerta("deseja Aceitar pontos",
							chamadaDePontosRobo, "Jogador Rôbo aumentou os pontos");

					controlaRodadaNegocio.ResponderAceitarOuNaoAceitarPontosHumano(chamadaDePontosRobo,
							respostaAceitaPontos);
					estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
					if (respostaAceitaPontos) {
						if (controlaRodadaNegocio.temPontosParaSerChamados(estadoJogoModelo)) {
							// verifica se jogador humâno quer aumentar
							boolean respostaQuerAumentar = new MessageAceitar().criaAlerta("deseja aumentar pontos",
									estadoJogoModelo.getListaPontosQuePodemSerAceitos(2), " você quer aumentar?");
							if (respostaQuerAumentar) {

								// se quiser aumentar habilita opções e permite escolher qual
								String pontoAumentado = new MessageListaOpcoesQuePodemSerChamadas().criaAlerta(
										"Qual ponto irá chamar?", estadoJogoModelo.listaPontosQuePodemSerChamados(),
										"Aumentar Pontos");
								controlaRodadaNegocio.AceitarPontosRobo(pontoChamado);
							}
						}
					}

				}

			}

		}

		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();

		QuemGanhouPontosModelo quemGanhou = controlaRodadaNegocio.pontuaQuemGanhouPontos();
		// mostra na tela quem ganhou
		if (quemGanhou.getQuemGanhou() == 1) {
			new InformationMensagem()
					.criaAlerta(
							"Pontuação rôbo: " + quemGanhou.getPontuacaoQuePossuiGanhador() + " sua pontuação: "
									+ quemGanhou.getPontuacaoQuepossuiQuemPerdeu(),
							"jogador "+quemGanhou.getQuemGanhou()+" ganhou os pontos", "ganhador pontos");
		} else if (!quemGanhou.equals(null) && quemGanhou.getQuemGanhou() == 2) {
			new InformationMensagem()
					.criaAlerta(
							"Pontuação rôbo: " + quemGanhou.getPontuacaoQuepossuiQuemPerdeu() + " sua pontuação: "
									+ quemGanhou.getPontuacaoQuePossuiGanhador(),
							"Você ganhou os pontos!", "ganhador pontos");
		}
		atualizarMarcadorPontos();
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
		//para dizer que já  passou por esse método
		return true;

	}

	// começa o rôbo perguntando para o humâno pontos
	public void perguntaAceitarPontosHumano() {
		controlaRodadaNegocio.chamarPontosRobo(estadoJogoModelo);
		controlaRodadaNegocio.setPontosJaChamados(true);
		List<String> listaPontosQuePodemSerAceitosPeloHumano = estadoJogoModelo.getListaPontosQuePodemSerAceitos(2);
		boolean respostaAceitaPontos = new MessageAceitar().criaAlerta("deseja Aceitar pontos",
				listaPontosQuePodemSerAceitosPeloHumano, "Jogador Rôbo chamou os pontos");

		controlaRodadaNegocio.ResponderAceitarOuNaoAceitarPontosHumano(
				listaPontosQuePodemSerAceitosPeloHumano.get(listaPontosQuePodemSerAceitosPeloHumano.size() - 2),
				respostaAceitaPontos);

		// se p quiser aceitar pontos passa para a rodada o aceite.
		if (respostaAceitaPontos) {

			// atualiza o estado do jogo
			estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();

			if (controlaRodadaNegocio.temPontosParaSerChamados(estadoJogoModelo)) {
				// verifica se jogador humâno quer aumentar
				boolean respostaQuerAumentar = new MessageAceitar().criaAlerta("deseja aumentar pontos",
						estadoJogoModelo.getListaPontosQuePodemSerAceitos(2),
						"Jogador Rôbo chamou, você quer aumentar?");
				if (respostaQuerAumentar) {

					// se quiser aumentar habilita opções e permite escolher qual
					String pontoChamado = new MessageListaOpcoesQuePodemSerChamadas().criaAlerta(
							"Qual ponto irá chamar?", estadoJogoModelo.listaPontosQuePodemSerChamados(),
							"Aumentar Pontos");
					controlaRodadaNegocio.AceitarPontosRobo(pontoChamado);
				}
			}
		}
		QuemGanhouPontosModelo quemGanhou = controlaRodadaNegocio.pontuaQuemGanhouPontos();
		// mostra na tela quem ganhou
		if (!quemGanhou.equals(null) && quemGanhou.getQuemGanhou() == 1) {
			new InformationMensagem()
					.criaAlerta(
							"Pontuação rôbo: " + quemGanhou.getPontuacaoQuePossuiGanhador() + " sua pontuação: "
									+ quemGanhou.getPontuacaoQuepossuiQuemPerdeu(),
							"Rôbo ganhou os pontos", "ganhador pontos");
		} else if (!quemGanhou.equals(null) && quemGanhou.getQuemGanhou() == 2) {
			new InformationMensagem()
					.criaAlerta(
							"Pontuação rôbo: " + quemGanhou.getPontuacaoQuepossuiQuemPerdeu() + " sua pontuação: "
									+ quemGanhou.getPontuacaoQuePossuiGanhador(),
							"Você ganhou os pontos!", "ganhador pontos");
		}
		atualizarMarcadorPontos();
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();

	}

	public void atualizarMarcadorPontos() {
		lblPontosRobo.setText(String.valueOf(controlaRodadaNegocio.getPontosRobo()));
		lblPontosHumano.setText(String.valueOf(controlaRodadaNegocio.getPontosHumano()));
	}

	private void verificaSeRoboQuejogaOuFimDeJogo() {
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
		if (controlaRodadaNegocio.aindaTemJogoNaRodada() && controlaRodadaNegocio.quemJogaAgora() == 1) {
			
			jogarRobo();
		}
		
		else if (estadoJogoModelo.isTemMaisRodada() && !controlaRodadaNegocio.aindaTemJogoNaRodada()) {
			controlaRodadaNegocio.pontuaQuemGanhouArodada();
			atualizarMarcadorPontos();
			IniciarRodada();
		} else if (!estadoJogoModelo.isTemMaisRodada()) {
			controlaRodadaNegocio.pontuaQuemGanhouArodada();
			atualizarMarcadorPontos();
			new InformationMensagem().criaAlerta("O jogo terminou!", "obrigado", "Fim de jogo!");

		}
	}

	private void verificaSeHumanoQuejogaOuFimDeJogo() {
		estadoJogoModelo = controlaRodadaNegocio.verificarEstadoJogo();
		if (controlaRodadaNegocio.aindaTemJogoNaRodada() && controlaRodadaNegocio.quemJogaAgora() == 2)
			jogarHumano();

		else if (estadoJogoModelo.isTemMaisRodada() && !controlaRodadaNegocio.aindaTemJogoNaRodada()) {
			controlaRodadaNegocio.pontuaQuemGanhouArodada();
			atualizarMarcadorPontos();
			IniciarRodada();
		} else if (!estadoJogoModelo.isTemMaisRodada()) {
			controlaRodadaNegocio.pontuaQuemGanhouArodada();
			atualizarMarcadorPontos();
			new InformationMensagem().criaAlerta("O jogo terminou!", "obrigado", "Fim de jogo!");

		}
	}

	public void iniciaTela() {
		lblAvisosJogo.setText("Ufsm");
		lblCartaDois.setText("cbr");
		lblCartaRobo.setText("IA");
		lblCartaTres.setText("game");
		lblCartaUm.setText("truco");
		lblChamadasRobo.setText("Virtual Gamer");
		lblRespostaRobo.setText("");

		cmbSelecionarCartaParaJogar.getItems().clear();
		cmbOqueChamar.getItems().clear();

		listViewHumano.getItems().clear();
		listViewRobo.getItems().clear();

		btnChamar.setDisable(true);
		btnIniciarRodada.setDisable(true);
		btnJogar.setDisable(true);
	}

}
